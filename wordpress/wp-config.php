<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jmtronik');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0>7JlCG}{d^|>]~iTk:c6`0m7u0$-9.pq89JR9P,j#v^-|>J)O,lN=<9%H510%yB');
define('SECURE_AUTH_KEY',  '$<X`f^d/ kh[8`L/MBnkkvQT-gkt><.dgM<>MTCz8gGI9;s,/+/A>`y*:=hI/mAn');
define('LOGGED_IN_KEY',    '_Gp(Y,=u+9WGJc}2S];ULA5we0p#5_0oz#1%NLe~Pd@E-Z~b/]Z0^{XE|l`AyiNK');
define('NONCE_KEY',        'F]<eu$Tj[gt{uZWG,&<Ficbc5Ug7AIXyFYTnC@yp<oW1>H)n{*#C.i,nrrPCVE#3');
define('AUTH_SALT',        '<hui&N[8+?Wm7sg#V6<>`5S.m64z$7~R|FarHi4MN-)8jM>R4?mCPQ8]Et+Dq2@+');
define('SECURE_AUTH_SALT', ':<6Ekr.v:p /IG7@iAd2/:~(Ne<uJEb$_vk~ u]z=.tnbEvD(Y,O1K=bd}9gL*BJ');
define('LOGGED_IN_SALT',   'g`P vkjrYXm.{,:5DsP6LsOI+,{R`i`3f]7A-+pIkDy:Ebg.6N,{;ZBxj$NI5p:=');
define('NONCE_SALT',       'wt%Y&m.gJZ2l~qxbj8#j<-I/g~|.6f2ul8=7,b&BQ^%8kH/DL;jT!Lw[<s!bI/EM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
