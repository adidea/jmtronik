/* DUPLICATOR-LITE (PHP BUILD MODE) MYSQL SCRIPT CREATED ON : 2018-09-03 22:18:03 */

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_duplicator_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `hash` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `owner` varchar(60) NOT NULL,
  `package` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=363 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=652 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


/* INSERT TABLE DATA: wp_comments */
INSERT INTO `wp_comments` VALUES("1", "1", "Komentator WordPress", "wapuu@wordpress.example", "https://wordpress.org/", "", "2018-08-28 12:20:05", "2018-08-28 10:20:05", "Cześć, to jest komentarz.\nAby zapoznać się z moderowaniem, edycją i usuwaniem komentarzy, należy odwiedzić ekran Komentarze w kokpicie.\nAwatary komentujących pochodzą z <a href=\"https://pl.gravatar.com\">Gravatara</a>.", "0", "post-trashed", "", "", "0", "0");

/* INSERT TABLE DATA: wp_duplicator_packages */
INSERT INTO `wp_duplicator_packages` VALUES("1", "20180903_jmtronik", "86f5068fe09c56599735180903221742", "20", "2018-09-03 22:18:03", "admin", "O:11:\"DUP_Package\":23:{s:7:\"Created\";s:19:\"2018-09-03 22:17:42\";s:7:\"Version\";s:6:\"1.2.42\";s:9:\"VersionWP\";s:5:\"4.9.8\";s:9:\"VersionDB\";s:6:\"5.7.21\";s:10:\"VersionPHP\";s:5:\"7.2.7\";s:9:\"VersionOS\";s:6:\"Darwin\";s:2:\"ID\";i:1;s:4:\"Name\";s:17:\"20180903_jmtronik\";s:4:\"Hash\";s:32:\"86f5068fe09c56599735180903221742\";s:8:\"NameHash\";s:50:\"20180903_jmtronik_86f5068fe09c56599735180903221742\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:83:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:40:\"http://localhost/wordpress/wp-snapshots/\";s:8:\"ScanFile\";s:60:\"20180903_jmtronik_86f5068fe09c56599735180903221742_scan.json\";s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";s:5:\"admin\";s:7:\"Archive\";O:11:\"DUP_Archive\":19:{s:10:\"FilterDirs\";s:0:\"\";s:11:\"FilterFiles\";s:0:\"\";s:10:\"FilterExts\";s:0:\"\";s:13:\"FilterDirsAll\";a:0:{}s:14:\"FilterFilesAll\";a:0:{}s:13:\"FilterExtsAll\";a:0:{}s:8:\"FilterOn\";i:0;s:12:\"ExportOnlyDB\";i:0;s:4:\"File\";s:62:\"20180903_jmtronik_86f5068fe09c56599735180903221742_archive.zip\";s:6:\"Format\";s:3:\"ZIP\";s:7:\"PackDir\";s:66:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress\";s:4:\"Size\";i:0;s:4:\"Dirs\";a:0:{}s:5:\"Files\";a:0:{}s:10:\"FilterInfo\";O:23:\"DUP_Archive_Filter_Info\":8:{s:4:\"Dirs\";O:34:\"DUP_Archive_Filter_Scope_Directory\":4:{s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:5:\"Files\";O:29:\"DUP_Archive_Filter_Scope_File\":5:{s:4:\"Size\";a:0:{}s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:4:\"Exts\";O:29:\"DUP_Archive_Filter_Scope_Base\":2:{s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:9:\"UDirCount\";i:0;s:10:\"UFileCount\";i:0;s:9:\"UExtCount\";i:0;s:8:\"TreeSize\";a:0:{}s:11:\"TreeWarning\";a:0:{}}s:14:\"RecursiveLinks\";a:0:{}s:10:\"\0*\0Package\";O:11:\"DUP_Package\":23:{s:7:\"Created\";s:19:\"2018-09-03 22:17:42\";s:7:\"Version\";s:6:\"1.2.42\";s:9:\"VersionWP\";s:5:\"4.9.8\";s:9:\"VersionDB\";s:6:\"5.7.21\";s:10:\"VersionPHP\";s:5:\"7.2.7\";s:9:\"VersionOS\";s:6:\"Darwin\";s:2:\"ID\";N;s:4:\"Name\";s:17:\"20180903_jmtronik\";s:4:\"Hash\";s:32:\"86f5068fe09c56599735180903221742\";s:8:\"NameHash\";s:50:\"20180903_jmtronik_86f5068fe09c56599735180903221742\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:83:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:40:\"http://localhost/wordpress/wp-snapshots/\";s:8:\"ScanFile\";N;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";N;s:7:\"Archive\";r:22;s:9:\"Installer\";O:13:\"DUP_Installer\":9:{s:4:\"File\";s:64:\"20180903_jmtronik_86f5068fe09c56599735180903221742_installer.php\";s:4:\"Size\";i:0;s:10:\"OptsDBHost\";s:0:\"\";s:10:\"OptsDBPort\";s:0:\"\";s:10:\"OptsDBName\";s:0:\"\";s:10:\"OptsDBUser\";s:0:\"\";s:12:\"OptsSecureOn\";i:0;s:14:\"OptsSecurePass\";s:0:\"\";s:10:\"\0*\0Package\";r:58;}s:8:\"Database\";O:12:\"DUP_Database\":13:{s:4:\"Type\";s:5:\"MySQL\";s:4:\"Size\";N;s:4:\"File\";s:63:\"20180903_jmtronik_86f5068fe09c56599735180903221742_database.sql\";s:4:\"Path\";N;s:12:\"FilterTables\";s:0:\"\";s:8:\"FilterOn\";i:0;s:4:\"Name\";N;s:10:\"Compatible\";s:0:\"\";s:8:\"Comments\";s:28:\"MySQL Community Server (GPL)\";s:10:\"\0*\0Package\";r:1;s:25:\"\0DUP_Database\0dbStorePath\";N;s:23:\"\0DUP_Database\0EOFMarker\";s:0:\"\";s:26:\"\0DUP_Database\0networkFlush\";b:0;}}s:29:\"\0DUP_Archive\0tmpFilterDirsAll\";a:0:{}s:24:\"\0DUP_Archive\0wpCorePaths\";a:6:{i:0;s:75:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-admin\";i:1;s:85:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/uploads\";i:2;s:87:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/languages\";i:3;s:85:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/plugins\";i:4;s:84:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/themes\";i:5;s:78:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-includes\";}}s:9:\"Installer\";r:80;s:8:\"Database\";r:90;}");

/* INSERT TABLE DATA: wp_options */
INSERT INTO `wp_options` VALUES("1", "siteurl", "http://localhost/wordpress", "yes");
INSERT INTO `wp_options` VALUES("2", "home", "http://localhost/wordpress", "yes");
INSERT INTO `wp_options` VALUES("3", "blogname", "JMTronik", "yes");
INSERT INTO `wp_options` VALUES("4", "blogdescription", "Kolejna witryna oparta na WordPressie", "yes");
INSERT INTO `wp_options` VALUES("5", "users_can_register", "0", "yes");
INSERT INTO `wp_options` VALUES("6", "admin_email", "kamil@studiograficzne.com", "yes");
INSERT INTO `wp_options` VALUES("7", "start_of_week", "1", "yes");
INSERT INTO `wp_options` VALUES("8", "use_balanceTags", "0", "yes");
INSERT INTO `wp_options` VALUES("9", "use_smilies", "1", "yes");
INSERT INTO `wp_options` VALUES("10", "require_name_email", "1", "yes");
INSERT INTO `wp_options` VALUES("11", "comments_notify", "1", "yes");
INSERT INTO `wp_options` VALUES("12", "posts_per_rss", "10", "yes");
INSERT INTO `wp_options` VALUES("13", "rss_use_excerpt", "0", "yes");
INSERT INTO `wp_options` VALUES("14", "mailserver_url", "mail.example.com", "yes");
INSERT INTO `wp_options` VALUES("15", "mailserver_login", "login@example.com", "yes");
INSERT INTO `wp_options` VALUES("16", "mailserver_pass", "password", "yes");
INSERT INTO `wp_options` VALUES("17", "mailserver_port", "110", "yes");
INSERT INTO `wp_options` VALUES("18", "default_category", "1", "yes");
INSERT INTO `wp_options` VALUES("19", "default_comment_status", "open", "yes");
INSERT INTO `wp_options` VALUES("20", "default_ping_status", "open", "yes");
INSERT INTO `wp_options` VALUES("21", "default_pingback_flag", "0", "yes");
INSERT INTO `wp_options` VALUES("22", "posts_per_page", "10", "yes");
INSERT INTO `wp_options` VALUES("23", "date_format", "j F Y", "yes");
INSERT INTO `wp_options` VALUES("24", "time_format", "H:i", "yes");
INSERT INTO `wp_options` VALUES("25", "links_updated_date_format", "j F Y H:i", "yes");
INSERT INTO `wp_options` VALUES("26", "comment_moderation", "0", "yes");
INSERT INTO `wp_options` VALUES("27", "moderation_notify", "1", "yes");
INSERT INTO `wp_options` VALUES("28", "permalink_structure", "/%year%/%monthnum%/%day%/%postname%/", "yes");
INSERT INTO `wp_options` VALUES("29", "rewrite_rules", "a:90:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}", "yes");
INSERT INTO `wp_options` VALUES("30", "hack_file", "0", "yes");
INSERT INTO `wp_options` VALUES("31", "blog_charset", "UTF-8", "yes");
INSERT INTO `wp_options` VALUES("32", "moderation_keys", "", "no");
INSERT INTO `wp_options` VALUES("33", "active_plugins", "a:3:{i:0;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:1;s:25:\"duplicator/duplicator.php\";i:2;s:26:\"snazzy-maps/snazzymaps.php\";}", "yes");
INSERT INTO `wp_options` VALUES("34", "category_base", "", "yes");
INSERT INTO `wp_options` VALUES("35", "ping_sites", "http://rpc.pingomatic.com/", "yes");
INSERT INTO `wp_options` VALUES("36", "comment_max_links", "2", "yes");
INSERT INTO `wp_options` VALUES("37", "gmt_offset", "0", "yes");
INSERT INTO `wp_options` VALUES("38", "default_email_category", "1", "yes");
INSERT INTO `wp_options` VALUES("39", "recently_edited", "", "no");
INSERT INTO `wp_options` VALUES("40", "template", "jmtronik", "yes");
INSERT INTO `wp_options` VALUES("41", "stylesheet", "jmtronik", "yes");
INSERT INTO `wp_options` VALUES("42", "comment_whitelist", "1", "yes");
INSERT INTO `wp_options` VALUES("43", "blacklist_keys", "", "no");
INSERT INTO `wp_options` VALUES("44", "comment_registration", "0", "yes");
INSERT INTO `wp_options` VALUES("45", "html_type", "text/html", "yes");
INSERT INTO `wp_options` VALUES("46", "use_trackback", "0", "yes");
INSERT INTO `wp_options` VALUES("47", "default_role", "subscriber", "yes");
INSERT INTO `wp_options` VALUES("48", "db_version", "38590", "yes");
INSERT INTO `wp_options` VALUES("49", "uploads_use_yearmonth_folders", "1", "yes");
INSERT INTO `wp_options` VALUES("50", "upload_path", "", "yes");
INSERT INTO `wp_options` VALUES("51", "blog_public", "0", "yes");
INSERT INTO `wp_options` VALUES("52", "default_link_category", "2", "yes");
INSERT INTO `wp_options` VALUES("53", "show_on_front", "page", "yes");
INSERT INTO `wp_options` VALUES("54", "tag_base", "", "yes");
INSERT INTO `wp_options` VALUES("55", "show_avatars", "1", "yes");
INSERT INTO `wp_options` VALUES("56", "avatar_rating", "G", "yes");
INSERT INTO `wp_options` VALUES("57", "upload_url_path", "", "yes");
INSERT INTO `wp_options` VALUES("58", "thumbnail_size_w", "150", "yes");
INSERT INTO `wp_options` VALUES("59", "thumbnail_size_h", "150", "yes");
INSERT INTO `wp_options` VALUES("60", "thumbnail_crop", "1", "yes");
INSERT INTO `wp_options` VALUES("61", "medium_size_w", "300", "yes");
INSERT INTO `wp_options` VALUES("62", "medium_size_h", "300", "yes");
INSERT INTO `wp_options` VALUES("63", "avatar_default", "mystery", "yes");
INSERT INTO `wp_options` VALUES("64", "large_size_w", "1024", "yes");
INSERT INTO `wp_options` VALUES("65", "large_size_h", "1024", "yes");
INSERT INTO `wp_options` VALUES("66", "image_default_link_type", "none", "yes");
INSERT INTO `wp_options` VALUES("67", "image_default_size", "", "yes");
INSERT INTO `wp_options` VALUES("68", "image_default_align", "", "yes");
INSERT INTO `wp_options` VALUES("69", "close_comments_for_old_posts", "0", "yes");
INSERT INTO `wp_options` VALUES("70", "close_comments_days_old", "14", "yes");
INSERT INTO `wp_options` VALUES("71", "thread_comments", "1", "yes");
INSERT INTO `wp_options` VALUES("72", "thread_comments_depth", "5", "yes");
INSERT INTO `wp_options` VALUES("73", "page_comments", "0", "yes");
INSERT INTO `wp_options` VALUES("74", "comments_per_page", "50", "yes");
INSERT INTO `wp_options` VALUES("75", "default_comments_page", "newest", "yes");
INSERT INTO `wp_options` VALUES("76", "comment_order", "asc", "yes");
INSERT INTO `wp_options` VALUES("77", "sticky_posts", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("78", "widget_categories", "a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("79", "widget_text", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("80", "widget_rss", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("81", "uninstall_plugins", "a:0:{}", "no");
INSERT INTO `wp_options` VALUES("82", "timezone_string", "Europe/Warsaw", "yes");
INSERT INTO `wp_options` VALUES("83", "page_for_posts", "0", "yes");
INSERT INTO `wp_options` VALUES("84", "page_on_front", "2", "yes");
INSERT INTO `wp_options` VALUES("85", "default_post_format", "0", "yes");
INSERT INTO `wp_options` VALUES("86", "link_manager_enabled", "0", "yes");
INSERT INTO `wp_options` VALUES("87", "finished_splitting_shared_terms", "1", "yes");
INSERT INTO `wp_options` VALUES("88", "site_icon", "0", "yes");
INSERT INTO `wp_options` VALUES("89", "medium_large_size_w", "768", "yes");
INSERT INTO `wp_options` VALUES("90", "medium_large_size_h", "0", "yes");
INSERT INTO `wp_options` VALUES("91", "wp_page_for_privacy_policy", "3", "yes");
INSERT INTO `wp_options` VALUES("92", "initial_db_version", "38590", "yes");
INSERT INTO `wp_options` VALUES("93", "wp_user_roles", "a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}", "yes");
INSERT INTO `wp_options` VALUES("94", "fresh_site", "0", "yes");
INSERT INTO `wp_options` VALUES("95", "WPLANG", "pl_PL", "yes");
INSERT INTO `wp_options` VALUES("96", "widget_search", "a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("97", "widget_recent-posts", "a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("98", "widget_recent-comments", "a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("99", "widget_archives", "a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("100", "widget_meta", "a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("101", "sidebars_widgets", "a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}", "yes");
INSERT INTO `wp_options` VALUES("102", "widget_pages", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("103", "widget_calendar", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("104", "widget_media_audio", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("105", "widget_media_image", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("106", "widget_media_gallery", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("107", "widget_media_video", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("108", "widget_tag_cloud", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("109", "widget_nav_menu", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("110", "widget_custom_html", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("111", "cron", "a:4:{i:1536013206;a:4:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1536056460;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1536071609;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}", "yes");
INSERT INTO `wp_options` VALUES("112", "theme_mods_twentyseventeen", "a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1535455074;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}", "yes");
INSERT INTO `wp_options` VALUES("122", "_site_transient_update_themes", "O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1536013044;s:7:\"checked\";a:1:{s:8:\"jmtronik\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}", "no");
INSERT INTO `wp_options` VALUES("124", "_site_transient_timeout_browser_807da36d6d362610e4cc4fc0e73bfcee", "1536056461", "no");
INSERT INTO `wp_options` VALUES("125", "_site_transient_browser_807da36d6d362610e4cc4fc0e73bfcee", "a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"68.0.3440.106\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}", "no");
INSERT INTO `wp_options` VALUES("129", "can_compress_scripts", "1", "no");
INSERT INTO `wp_options` VALUES("140", "recently_activated", "a:1:{s:30:\"advanced-custom-fields/acf.php\";i:1535451805;}", "yes");
INSERT INTO `wp_options` VALUES("141", "acf_version", "5.7.2", "yes");
INSERT INTO `wp_options` VALUES("152", "acf_pro_license", "YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TnpFM09UVjhkSGx3WlQxa1pYWmxiRzl3WlhKOFpHRjBaVDB5TURFMUxURXlMVE14SURFeU9qUTFPakV4IjtzOjM6InVybCI7czoyNjoiaHR0cDovL2xvY2FsaG9zdC93b3JkcHJlc3MiO30=", "yes");
INSERT INTO `wp_options` VALUES("156", "current_theme", "jmtronik", "yes");
INSERT INTO `wp_options` VALUES("157", "theme_mods_jmtronik", "a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:11:\"header-menu\";i:2;s:11:\"footer-menu\";i:0;}s:18:\"custom_css_post_id\";i:-1;}", "yes");
INSERT INTO `wp_options` VALUES("158", "theme_switched", "", "yes");
INSERT INTO `wp_options` VALUES("166", "nav_menu_options", "a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}", "yes");
INSERT INTO `wp_options` VALUES("204", "options_footer_menu_links_0_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("205", "_options_footer_menu_links_0_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("206", "options_footer_menu_links_0_footer_menu_link_desc", "Strona 1", "no");
INSERT INTO `wp_options` VALUES("207", "_options_footer_menu_links_0_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("208", "options_footer_menu_links", "7", "no");
INSERT INTO `wp_options` VALUES("209", "_options_footer_menu_links", "field_5b88005089227", "no");
INSERT INTO `wp_options` VALUES("210", "options_footer_menu_links_1_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("211", "_options_footer_menu_links_1_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("212", "options_footer_menu_links_1_footer_menu_link_desc", "Strona 2", "no");
INSERT INTO `wp_options` VALUES("213", "_options_footer_menu_links_1_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("214", "options_footer_menu_links_2_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("215", "_options_footer_menu_links_2_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("216", "options_footer_menu_links_2_footer_menu_link_desc", "Strona 3", "no");
INSERT INTO `wp_options` VALUES("217", "_options_footer_menu_links_2_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("218", "options_footer_menu_links_3_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("219", "_options_footer_menu_links_3_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("220", "options_footer_menu_links_3_footer_menu_link_desc", "Strona 4", "no");
INSERT INTO `wp_options` VALUES("221", "_options_footer_menu_links_3_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("222", "options_footer_menu_links_4_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("223", "_options_footer_menu_links_4_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("224", "options_footer_menu_links_4_footer_menu_link_desc", "Strona 5", "no");
INSERT INTO `wp_options` VALUES("225", "_options_footer_menu_links_4_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("226", "options_footer_menu_links_5_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("227", "_options_footer_menu_links_5_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("228", "options_footer_menu_links_5_footer_menu_link_desc", "Strona 6", "no");
INSERT INTO `wp_options` VALUES("229", "_options_footer_menu_links_5_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("230", "options_footer_menu_links_6_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("231", "_options_footer_menu_links_6_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("232", "options_footer_menu_links_6_footer_menu_link_desc", "Strona 7", "no");
INSERT INTO `wp_options` VALUES("233", "_options_footer_menu_links_6_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("243", "_site_transient_update_core", "O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"pl_PL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-4.9.8.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1536013043;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}", "no");
INSERT INTO `wp_options` VALUES("244", "options_footer_menu_links_1_0_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("245", "_options_footer_menu_links_1_0_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("246", "options_footer_menu_links_1_0_footer_menu_link_desc", "Link 1", "no");
INSERT INTO `wp_options` VALUES("247", "_options_footer_menu_links_1_0_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("248", "options_footer_menu_links_1", "5", "no");
INSERT INTO `wp_options` VALUES("249", "_options_footer_menu_links_1", "field_5b88005089227", "no");
INSERT INTO `wp_options` VALUES("250", "options_footer_menu_links_2_0_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("251", "_options_footer_menu_links_2_0_footer_menu_link", "field_5b8905d43685d", "no");
INSERT INTO `wp_options` VALUES("252", "options_footer_menu_links_2_0_footer_menu_link_desc", "Link 2", "no");
INSERT INTO `wp_options` VALUES("253", "_options_footer_menu_links_2_0_footer_menu_link_desc", "field_5b8905d43685e", "no");
INSERT INTO `wp_options` VALUES("254", "options_footer_menu_links_2", "1", "no");
INSERT INTO `wp_options` VALUES("255", "_options_footer_menu_links_2", "field_5b8905d43685c", "no");
INSERT INTO `wp_options` VALUES("256", "options_footer_addres", "JM-TRONIC Sp. z o.o.\r\n\r\nJM-TRONIC Sp. z o.o.\r\n\r\nJM-TRONIC Sp. z o.o.\r\n\r\nJM-TRONIC Sp. z o.o.", "no");
INSERT INTO `wp_options` VALUES("257", "_options_footer_addres", "field_5b890615879e2", "no");
INSERT INTO `wp_options` VALUES("258", "options_footer_contact", "biuro@jmtronik.pl\r\n\r\nbiuro@jmtronik.pl\r\n\r\nbiuro@jmtronik.pl", "no");
INSERT INTO `wp_options` VALUES("259", "_options_footer_contact", "field_5b8907ccff045", "no");
INSERT INTO `wp_options` VALUES("261", "options_social_facebook", "http://facebook.pl", "no");
INSERT INTO `wp_options` VALUES("262", "_options_social_facebook", "field_5b890a7bc34a0", "no");
INSERT INTO `wp_options` VALUES("263", "options_social_google", "", "no");
INSERT INTO `wp_options` VALUES("264", "_options_social_google", "field_5b890ac4c34a1", "no");
INSERT INTO `wp_options` VALUES("265", "options_social_twitter", "", "no");
INSERT INTO `wp_options` VALUES("266", "_options_social_twitter", "field_5b890adac34a2", "no");
INSERT INTO `wp_options` VALUES("267", "options_social_youtube", "", "no");
INSERT INTO `wp_options` VALUES("268", "_options_social_youtube", "field_5b890aeac34a3", "no");
INSERT INTO `wp_options` VALUES("269", "options_footer_menu_links_1_1_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("270", "_options_footer_menu_links_1_1_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("271", "options_footer_menu_links_1_1_footer_menu_link_desc", "Link 2", "no");
INSERT INTO `wp_options` VALUES("272", "_options_footer_menu_links_1_1_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("273", "options_footer_menu_links_1_2_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("274", "_options_footer_menu_links_1_2_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("275", "options_footer_menu_links_1_2_footer_menu_link_desc", "Link 3", "no");
INSERT INTO `wp_options` VALUES("276", "_options_footer_menu_links_1_2_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("277", "options_footer_menu_links_1_3_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("278", "_options_footer_menu_links_1_3_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("279", "options_footer_menu_links_1_3_footer_menu_link_desc", "Link 4", "no");
INSERT INTO `wp_options` VALUES("280", "_options_footer_menu_links_1_3_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("281", "options_footer_menu_links_1_4_footer_menu_link", "2", "no");
INSERT INTO `wp_options` VALUES("282", "_options_footer_menu_links_1_4_footer_menu_link", "field_5b8800bc89229", "no");
INSERT INTO `wp_options` VALUES("283", "options_footer_menu_links_1_4_footer_menu_link_desc", "DO POBRANIA", "no");
INSERT INTO `wp_options` VALUES("284", "_options_footer_menu_links_1_4_footer_menu_link_desc", "field_5b8800f68922a", "no");
INSERT INTO `wp_options` VALUES("318", "SnazzyMapStyles", "a:1:{i:0;a:12:{s:2:\"id\";i:6344;s:4:\"name\";s:5:\"title\";s:11:\"description\";s:22:\"titleboxingclub colors\";s:3:\"url\";s:39:\"https://snazzymaps.com/style/6344/title\";s:8:\"imageUrl\";s:76:\"https://snazzy-maps-cdn.azureedge.net/assets/6344-title.png?v=20170626060637\";s:4:\"json\";s:1400:\"[{\"featureType\":\"administrative\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#444444\"}]},{\"featureType\":\"administrative.neighborhood\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"visibility\":\"off\"},{\"hue\":\"#17ff00\"}]},{\"featureType\":\"administrative.neighborhood\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"landscape\",\"elementType\":\"all\",\"stylers\":[{\"color\":\"#f2f2f2\"}]},{\"featureType\":\"poi\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"labels.text\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road\",\"elementType\":\"all\",\"stylers\":[{\"saturation\":-100},{\"lightness\":45}]},{\"featureType\":\"road.highway\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"simplified\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#ff0000\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"transit\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"all\",\"stylers\":[{\"color\":\"#0088cc\"},{\"visibility\":\"on\"}]}]\";s:5:\"views\";i:4536;s:9:\"favorites\";i:35;s:9:\"createdBy\";a:2:{s:4:\"name\";s:7:\"lindsey\";s:3:\"url\";N;}s:9:\"createdOn\";s:23:\"2015-01-26T17:59:28.557\";s:4:\"tags\";a:1:{i:0;s:6:\"simple\";}s:6:\"colors\";a:3:{i:0;s:5:\"black\";i:1;s:4:\"blue\";i:2;s:3:\"red\";}}}", "yes");
INSERT INTO `wp_options` VALUES("319", "SnazzyMapDefaultStyle", "a:12:{s:2:\"id\";i:6344;s:4:\"name\";s:5:\"title\";s:11:\"description\";s:22:\"titleboxingclub colors\";s:3:\"url\";s:39:\"https://snazzymaps.com/style/6344/title\";s:8:\"imageUrl\";s:76:\"https://snazzy-maps-cdn.azureedge.net/assets/6344-title.png?v=20170626060637\";s:4:\"json\";s:1400:\"[{\"featureType\":\"administrative\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"color\":\"#444444\"}]},{\"featureType\":\"administrative.neighborhood\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"visibility\":\"off\"},{\"hue\":\"#17ff00\"}]},{\"featureType\":\"administrative.neighborhood\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"landscape\",\"elementType\":\"all\",\"stylers\":[{\"color\":\"#f2f2f2\"}]},{\"featureType\":\"poi\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"labels.text\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"poi.business\",\"elementType\":\"labels.text.fill\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road\",\"elementType\":\"all\",\"stylers\":[{\"saturation\":-100},{\"lightness\":45}]},{\"featureType\":\"road.highway\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"simplified\"}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#ff0000\"}]},{\"featureType\":\"road.arterial\",\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"visibility\":\"on\"}]},{\"featureType\":\"transit\",\"elementType\":\"all\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"all\",\"stylers\":[{\"color\":\"#0088cc\"},{\"visibility\":\"on\"}]}]\";s:5:\"views\";i:4536;s:9:\"favorites\";i:35;s:9:\"createdBy\";a:2:{s:4:\"name\";s:7:\"lindsey\";s:3:\"url\";N;}s:9:\"createdOn\";s:23:\"2015-01-26T17:59:28.557\";s:4:\"tags\";a:1:{i:0;s:6:\"simple\";}s:6:\"colors\";a:3:{i:0;s:5:\"black\";i:1;s:4:\"blue\";i:2;s:3:\"red\";}}", "yes");
INSERT INTO `wp_options` VALUES("322", "options_maps_api_key", "AIzaSyCnBXzulb2wA3jNI60n9ARsgA4mZ_reJSg", "no");
INSERT INTO `wp_options` VALUES("323", "_options_maps_api_key", "field_5b8aa379e6a24", "no");
INSERT INTO `wp_options` VALUES("344", "category_children", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("347", "_site_transient_timeout_theme_roots", "1536013440", "no");
INSERT INTO `wp_options` VALUES("348", "_site_transient_theme_roots", "a:1:{s:8:\"jmtronik\";s:7:\"/themes\";}", "no");
INSERT INTO `wp_options` VALUES("352", "_transient_timeout_acf_plugin_updates", "1536099419", "no");
INSERT INTO `wp_options` VALUES("353", "_transient_acf_plugin_updates", "a:4:{s:7:\"plugins\";a:1:{s:41:\"advanced-custom-fields-pro-master/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:11:\"new_version\";s:5:\"5.7.4\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:5:\"4.9.9\";s:7:\"package\";s:243:\"https://connect.advancedcustomfields.com/v2/plugins/download?token=eyJwIjoicHJvIiwiayI6ImIzSmtaWEpmYVdROU56RTNPVFY4ZEhsd1pUMWtaWFpsYkc5d1pYSjhaR0YwWlQweU1ERTFMVEV5TFRNeElERXlPalExT2pFeCIsIndwX3VybCI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvd29yZHByZXNzIn0=\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:10:\"expiration\";i:86400;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:41:\"advanced-custom-fields-pro-master/acf.php\";s:5:\"5.7.2\";}}", "no");
INSERT INTO `wp_options` VALUES("354", "_transient_timeout_plugin_slugs", "1536099448", "no");
INSERT INTO `wp_options` VALUES("355", "_transient_plugin_slugs", "a:6:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:2;s:19:\"akismet/akismet.php\";i:3;s:25:\"duplicator/duplicator.php\";i:4;s:9:\"hello.php\";i:5;s:26:\"snazzy-maps/snazzymaps.php\";}", "no");
INSERT INTO `wp_options` VALUES("356", "_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a", "1536023824", "no");
INSERT INTO `wp_options` VALUES("357", "_site_transient_poptags_40cd750bba9870f18aada2478b24840a", "O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4500;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:2964;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2584;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2448;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1884;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1678;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1671;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1458;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1404;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1400;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1395;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1333;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1284;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1241;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1113;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1072;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1038;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1037;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:920;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:891;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:834;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:813;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:807;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:733;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:704;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:696;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:689;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:687;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:676;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:667;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:658;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:657;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:646;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:644;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:616;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:616;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:614;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:606;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:600;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:597;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:576;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:551;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:545;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:542;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:532;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:525;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:516;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:515;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:515;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:509;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:495;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:494;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:492;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:486;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:481;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:480;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:460;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:458;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:453;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:443;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:440;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:437;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:422;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:420;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:417;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:417;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:416;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:414;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:409;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:389;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:388;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:374;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:373;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:371;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:366;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:364;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:360;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:359;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:353;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:347;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:347;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:346;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:345;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:339;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:337;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:334;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:329;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:327;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:312;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:309;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:309;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:306;}s:3:\"tag\";a:3:{s:4:\"name\";s:3:\"tag\";s:4:\"slug\";s:3:\"tag\";s:5:\"count\";i:303;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:303;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:303;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:301;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:299;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:298;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:298;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:294;}}", "no");
INSERT INTO `wp_options` VALUES("359", "_site_transient_update_plugins", "O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1536013047;s:7:\"checked\";a:6:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"4.4.12\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:5:\"5.7.2\";s:19:\"akismet/akismet.php\";s:5:\"4.0.3\";s:25:\"duplicator/duplicator.php\";s:6:\"1.2.42\";s:9:\"hello.php\";s:3:\"1.7\";s:26:\"snazzy-maps/snazzymaps.php\";s:5:\"1.1.5\";}s:8:\"response\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:41:\"advanced-custom-fields-pro-master/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:11:\"new_version\";s:5:\"5.7.4\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:5:\"4.9.9\";s:7:\"package\";s:243:\"https://connect.advancedcustomfields.com/v2/plugins/download?token=eyJwIjoicHJvIiwiayI6ImIzSmtaWEpmYVdROU56RTNPVFY4ZEhsd1pUMWtaWFpsYkc5d1pYSjhaR0YwWlQweU1ERTFMVEV5TFRNeElERXlPalExT2pFeCIsIndwX3VybCI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvd29yZHByZXNzIn0=\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"duplicator/duplicator.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/duplicator\";s:4:\"slug\";s:10:\"duplicator\";s:6:\"plugin\";s:25:\"duplicator/duplicator.php\";s:11:\"new_version\";s:6:\"1.2.42\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/duplicator/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/duplicator.1.2.42.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/duplicator/assets/icon-256x256.png?rev=1298463\";s:2:\"1x\";s:63:\"https://ps.w.org/duplicator/assets/icon-128x128.png?rev=1298463\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/duplicator/assets/banner-772x250.png?rev=1645055\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:26:\"snazzy-maps/snazzymaps.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/snazzy-maps\";s:4:\"slug\";s:11:\"snazzy-maps\";s:6:\"plugin\";s:26:\"snazzy-maps/snazzymaps.php\";s:11:\"new_version\";s:5:\"1.1.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/snazzy-maps/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/snazzy-maps.1.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/snazzy-maps/assets/icon-256x256.png?rev=1130667\";s:2:\"1x\";s:64:\"https://ps.w.org/snazzy-maps/assets/icon-128x128.png?rev=1130680\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/snazzy-maps/assets/banner-1544x500.png?rev=1102757\";s:2:\"1x\";s:66:\"https://ps.w.org/snazzy-maps/assets/banner-772x250.png?rev=1102757\";}s:11:\"banners_rtl\";a:0:{}}}}", "no");
INSERT INTO `wp_options` VALUES("360", "duplicator_settings", "a:10:{s:7:\"version\";s:6:\"1.2.42\";s:18:\"uninstall_settings\";b:1;s:15:\"uninstall_files\";b:1;s:16:\"uninstall_tables\";b:1;s:13:\"package_debug\";b:0;s:17:\"package_mysqldump\";b:1;s:22:\"package_mysqldump_path\";s:0:\"\";s:24:\"package_phpdump_qrylimit\";s:3:\"100\";s:17:\"package_zip_flush\";b:0;s:20:\"storage_htaccess_off\";b:0;}", "yes");
INSERT INTO `wp_options` VALUES("361", "duplicator_version_plugin", "1.2.42", "yes");
INSERT INTO `wp_options` VALUES("362", "duplicator_package_active", "O:11:\"DUP_Package\":23:{s:7:\"Created\";s:19:\"2018-09-03 22:17:42\";s:7:\"Version\";s:6:\"1.2.42\";s:9:\"VersionWP\";s:5:\"4.9.8\";s:9:\"VersionDB\";s:6:\"5.7.21\";s:10:\"VersionPHP\";s:5:\"7.2.7\";s:9:\"VersionOS\";s:6:\"Darwin\";s:2:\"ID\";N;s:4:\"Name\";s:17:\"20180903_jmtronik\";s:4:\"Hash\";s:32:\"86f5068fe09c56599735180903221742\";s:8:\"NameHash\";s:50:\"20180903_jmtronik_86f5068fe09c56599735180903221742\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:83:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:40:\"http://localhost/wordpress/wp-snapshots/\";s:8:\"ScanFile\";s:60:\"20180903_jmtronik_86f5068fe09c56599735180903221742_scan.json\";s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";N;s:7:\"Archive\";O:11:\"DUP_Archive\":19:{s:10:\"FilterDirs\";s:0:\"\";s:11:\"FilterFiles\";s:0:\"\";s:10:\"FilterExts\";s:0:\"\";s:13:\"FilterDirsAll\";a:0:{}s:14:\"FilterFilesAll\";a:0:{}s:13:\"FilterExtsAll\";a:0:{}s:8:\"FilterOn\";i:0;s:12:\"ExportOnlyDB\";i:0;s:4:\"File\";N;s:6:\"Format\";s:3:\"ZIP\";s:7:\"PackDir\";s:66:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress\";s:4:\"Size\";i:0;s:4:\"Dirs\";a:0:{}s:5:\"Files\";a:0:{}s:10:\"FilterInfo\";O:23:\"DUP_Archive_Filter_Info\":8:{s:4:\"Dirs\";O:34:\"DUP_Archive_Filter_Scope_Directory\":4:{s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:5:\"Files\";O:29:\"DUP_Archive_Filter_Scope_File\":5:{s:4:\"Size\";a:0:{}s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:4:\"Exts\";O:29:\"DUP_Archive_Filter_Scope_Base\":2:{s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:9:\"UDirCount\";i:0;s:10:\"UFileCount\";i:0;s:9:\"UExtCount\";i:0;s:8:\"TreeSize\";a:0:{}s:11:\"TreeWarning\";a:0:{}}s:14:\"RecursiveLinks\";a:0:{}s:10:\"\0*\0Package\";O:11:\"DUP_Package\":23:{s:7:\"Created\";s:19:\"2018-09-03 22:17:42\";s:7:\"Version\";s:6:\"1.2.42\";s:9:\"VersionWP\";s:5:\"4.9.8\";s:9:\"VersionDB\";s:6:\"5.7.21\";s:10:\"VersionPHP\";s:5:\"7.2.7\";s:9:\"VersionOS\";s:6:\"Darwin\";s:2:\"ID\";N;s:4:\"Name\";s:17:\"20180903_jmtronik\";s:4:\"Hash\";s:32:\"86f5068fe09c56599735180903221742\";s:8:\"NameHash\";s:50:\"20180903_jmtronik_86f5068fe09c56599735180903221742\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:83:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:40:\"http://localhost/wordpress/wp-snapshots/\";s:8:\"ScanFile\";N;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";N;s:7:\"Archive\";r:22;s:9:\"Installer\";O:13:\"DUP_Installer\":9:{s:4:\"File\";N;s:4:\"Size\";i:0;s:10:\"OptsDBHost\";s:0:\"\";s:10:\"OptsDBPort\";s:0:\"\";s:10:\"OptsDBName\";s:0:\"\";s:10:\"OptsDBUser\";s:0:\"\";s:12:\"OptsSecureOn\";i:0;s:14:\"OptsSecurePass\";s:0:\"\";s:10:\"\0*\0Package\";r:58;}s:8:\"Database\";O:12:\"DUP_Database\":13:{s:4:\"Type\";s:5:\"MySQL\";s:4:\"Size\";N;s:4:\"File\";N;s:4:\"Path\";N;s:12:\"FilterTables\";s:0:\"\";s:8:\"FilterOn\";i:0;s:4:\"Name\";N;s:10:\"Compatible\";s:0:\"\";s:8:\"Comments\";s:28:\"MySQL Community Server (GPL)\";s:10:\"\0*\0Package\";r:58;s:25:\"\0DUP_Database\0dbStorePath\";N;s:23:\"\0DUP_Database\0EOFMarker\";s:0:\"\";s:26:\"\0DUP_Database\0networkFlush\";b:0;}}s:29:\"\0DUP_Archive\0tmpFilterDirsAll\";a:0:{}s:24:\"\0DUP_Archive\0wpCorePaths\";a:6:{i:0;s:75:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-admin\";i:1;s:85:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/uploads\";i:2;s:87:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/languages\";i:3;s:85:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/plugins\";i:4;s:84:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-content/themes\";i:5;s:78:\"/Users/Kamil/Documents/klienci/Studio Graficzne/jmtronik/wordpress/wp-includes\";}}s:9:\"Installer\";r:80;s:8:\"Database\";r:90;}", "yes");

/* INSERT TABLE DATA: wp_postmeta */
INSERT INTO `wp_postmeta` VALUES("1", "2", "_wp_page_template", "page-main.php");
INSERT INTO `wp_postmeta` VALUES("2", "3", "_wp_page_template", "default");
INSERT INTO `wp_postmeta` VALUES("5", "6", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("6", "6", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("7", "6", "_menu_item_object_id", "2");
INSERT INTO `wp_postmeta` VALUES("8", "6", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("9", "6", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("10", "6", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("11", "6", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("12", "6", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("14", "7", "_wp_attached_file", "2018/08/Warstwa-2.png");
INSERT INTO `wp_postmeta` VALUES("15", "7", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:268;s:6:\"height\";i:49;s:4:\"file\";s:21:\"2018/08/Warstwa-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Warstwa-2-150x49.png\";s:5:\"width\";i:150;s:6:\"height\";i:49;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("16", "8", "_wp_attached_file", "2018/08/logo-white.png");
INSERT INTO `wp_postmeta` VALUES("17", "8", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:234;s:6:\"height\";i:42;s:4:\"file\";s:22:\"2018/08/logo-white.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"logo-white-150x42.png\";s:5:\"width\";i:150;s:6:\"height\";i:42;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("98", "18", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("99", "18", "_edit_lock", "1535707583:1");
INSERT INTO `wp_postmeta` VALUES("100", "31", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("101", "31", "_edit_lock", "1535912060:1");
INSERT INTO `wp_postmeta` VALUES("102", "37", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("103", "37", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("104", "37", "_menu_item_object_id", "37");
INSERT INTO `wp_postmeta` VALUES("105", "37", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("106", "37", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("107", "37", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("108", "37", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("109", "37", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("111", "38", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("112", "38", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("113", "38", "_menu_item_object_id", "38");
INSERT INTO `wp_postmeta` VALUES("114", "38", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("115", "38", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("116", "38", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("117", "38", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("118", "38", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("120", "39", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("121", "39", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("122", "39", "_menu_item_object_id", "39");
INSERT INTO `wp_postmeta` VALUES("123", "39", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("124", "39", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("125", "39", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("126", "39", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("127", "39", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("129", "40", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("130", "40", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("131", "40", "_menu_item_object_id", "40");
INSERT INTO `wp_postmeta` VALUES("132", "40", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("133", "40", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("134", "40", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("135", "40", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("136", "40", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("138", "41", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("139", "41", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("140", "41", "_menu_item_object_id", "41");
INSERT INTO `wp_postmeta` VALUES("141", "41", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("142", "41", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("143", "41", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("144", "41", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("145", "41", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("147", "42", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("148", "42", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("149", "42", "_menu_item_object_id", "42");
INSERT INTO `wp_postmeta` VALUES("150", "42", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("151", "42", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("152", "42", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("153", "42", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("154", "42", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("156", "2", "_edit_lock", "1536012875:1");
INSERT INTO `wp_postmeta` VALUES("157", "2", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("158", "44", "_wp_attached_file", "2018/08/main_page_bg.png");
INSERT INTO `wp_postmeta` VALUES("159", "44", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:1719;s:6:\"height\";i:1485;s:4:\"file\";s:24:\"2018/08/main_page_bg.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"main_page_bg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"main_page_bg-300x259.png\";s:5:\"width\";i:300;s:6:\"height\";i:259;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"main_page_bg-768x663.png\";s:5:\"width\";i:768;s:6:\"height\";i:663;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"main_page_bg-1024x885.png\";s:5:\"width\";i:1024;s:6:\"height\";i:885;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("160", "45", "_wp_attached_file", "2018/08/Warstwa-32product.png");
INSERT INTO `wp_postmeta` VALUES("161", "45", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:256;s:6:\"height\";i:217;s:4:\"file\";s:29:\"2018/08/Warstwa-32product.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"Warstwa-32product-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("162", "46", "_wp_attached_file", "2018/08/main_bg_2.png");
INSERT INTO `wp_postmeta` VALUES("163", "46", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:799;s:6:\"height\";i:1333;s:4:\"file\";s:21:\"2018/08/main_bg_2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"main_bg_2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"main_bg_2-180x300.png\";s:5:\"width\";i:180;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"main_bg_2-768x1281.png\";s:5:\"width\";i:768;s:6:\"height\";i:1281;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"main_bg_2-614x1024.png\";s:5:\"width\";i:614;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("164", "47", "_wp_attached_file", "2018/09/Warstwa-19brand.png");
INSERT INTO `wp_postmeta` VALUES("165", "47", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:188;s:6:\"height\";i:109;s:4:\"file\";s:27:\"2018/09/Warstwa-19brand.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"Warstwa-19brand-150x109.png\";s:5:\"width\";i:150;s:6:\"height\";i:109;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("166", "49", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("167", "49", "_edit_lock", "1536012778:1");
INSERT INTO `wp_postmeta` VALUES("168", "2", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("169", "2", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("170", "2", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("171", "2", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("172", "52", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("173", "52", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("174", "52", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("175", "52", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("176", "2", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("177", "2", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("178", "2", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("179", "2", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("180", "2", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("181", "2", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("182", "2", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("183", "2", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("184", "2", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("185", "2", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("186", "2", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("187", "2", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("188", "2", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("189", "2", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("190", "2", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("191", "2", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("192", "63", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("193", "63", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("194", "63", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("195", "63", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("196", "63", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("197", "63", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("198", "63", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("199", "63", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("200", "63", "main_sec_1_heading_red", "");
INSERT INTO `wp_postmeta` VALUES("201", "63", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("202", "63", "main_sec_1_heading", "");
INSERT INTO `wp_postmeta` VALUES("203", "63", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("204", "63", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("205", "63", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("206", "63", "main_sec_1_heading_about", "");
INSERT INTO `wp_postmeta` VALUES("207", "63", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("208", "63", "main_sec_1_text_about", "");
INSERT INTO `wp_postmeta` VALUES("209", "63", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("210", "63", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("211", "63", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("212", "64", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("213", "64", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("214", "64", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("215", "64", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("216", "64", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("217", "64", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("218", "64", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("219", "64", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("220", "64", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("221", "64", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("222", "64", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("223", "64", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("224", "64", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("225", "64", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("226", "64", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("227", "64", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("228", "64", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("229", "64", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("230", "64", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("231", "64", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("232", "2", "main_sec_2_products_0_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("233", "2", "_main_sec_2_products_0_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("234", "2", "main_sec_2_products_0_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("235", "2", "_main_sec_2_products_0_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("236", "2", "main_sec_2_products_0_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("237", "2", "_main_sec_2_products_0_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("238", "2", "main_sec_2_products_1_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("239", "2", "_main_sec_2_products_1_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("240", "2", "main_sec_2_products_1_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("241", "2", "_main_sec_2_products_1_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("242", "2", "main_sec_2_products_1_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("243", "2", "_main_sec_2_products_1_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("244", "2", "main_sec_2_products_2_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("245", "2", "_main_sec_2_products_2_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("246", "2", "main_sec_2_products_2_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("247", "2", "_main_sec_2_products_2_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("248", "2", "main_sec_2_products_2_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("249", "2", "_main_sec_2_products_2_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("250", "2", "main_sec_2_products_3_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("251", "2", "_main_sec_2_products_3_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("252", "2", "main_sec_2_products_3_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("253", "2", "_main_sec_2_products_3_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("254", "2", "main_sec_2_products_3_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("255", "2", "_main_sec_2_products_3_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("256", "2", "main_sec_2_products_4_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("257", "2", "_main_sec_2_products_4_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("258", "2", "main_sec_2_products_4_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("259", "2", "_main_sec_2_products_4_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("260", "2", "main_sec_2_products_4_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("261", "2", "_main_sec_2_products_4_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("262", "2", "main_sec_2_products", "5");
INSERT INTO `wp_postmeta` VALUES("263", "2", "_main_sec_2_products", "field_5b8d97e92fe3b");
INSERT INTO `wp_postmeta` VALUES("264", "70", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("265", "70", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("266", "70", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("267", "70", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("268", "70", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("269", "70", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("270", "70", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("271", "70", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("272", "70", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("273", "70", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("274", "70", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("275", "70", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("276", "70", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("277", "70", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("278", "70", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("279", "70", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("280", "70", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("281", "70", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("282", "70", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("283", "70", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("284", "70", "main_sec_2_products_0_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("285", "70", "_main_sec_2_products_0_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("286", "70", "main_sec_2_products_0_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("287", "70", "_main_sec_2_products_0_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("288", "70", "main_sec_2_products_0_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("289", "70", "_main_sec_2_products_0_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("290", "70", "main_sec_2_products_1_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("291", "70", "_main_sec_2_products_1_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("292", "70", "main_sec_2_products_1_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("293", "70", "_main_sec_2_products_1_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("294", "70", "main_sec_2_products_1_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("295", "70", "_main_sec_2_products_1_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("296", "70", "main_sec_2_products_2_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("297", "70", "_main_sec_2_products_2_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("298", "70", "main_sec_2_products_2_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("299", "70", "_main_sec_2_products_2_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("300", "70", "main_sec_2_products_2_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("301", "70", "_main_sec_2_products_2_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("302", "70", "main_sec_2_products_3_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("303", "70", "_main_sec_2_products_3_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("304", "70", "main_sec_2_products_3_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("305", "70", "_main_sec_2_products_3_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("306", "70", "main_sec_2_products_3_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("307", "70", "_main_sec_2_products_3_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("308", "70", "main_sec_2_products_4_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("309", "70", "_main_sec_2_products_4_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("310", "70", "main_sec_2_products_4_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("311", "70", "_main_sec_2_products_4_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("312", "70", "main_sec_2_products_4_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("313", "70", "_main_sec_2_products_4_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("314", "70", "main_sec_2_products", "5");
INSERT INTO `wp_postmeta` VALUES("315", "70", "_main_sec_2_products", "field_5b8d97e92fe3b");
INSERT INTO `wp_postmeta` VALUES("316", "2", "main_sec_2_products_0_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("317", "2", "_main_sec_2_products_0_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("318", "2", "main_sec_2_products_1_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("319", "2", "_main_sec_2_products_1_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("320", "2", "main_sec_2_products_2_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("321", "2", "_main_sec_2_products_2_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("322", "2", "main_sec_2_products_3_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("323", "2", "_main_sec_2_products_3_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("324", "2", "main_sec_2_products_4_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("325", "2", "_main_sec_2_products_4_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("326", "71", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("327", "71", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("328", "71", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("329", "71", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("330", "71", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("331", "71", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("332", "71", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("333", "71", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("334", "71", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("335", "71", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("336", "71", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("337", "71", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("338", "71", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("339", "71", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("340", "71", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("341", "71", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("342", "71", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("343", "71", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("344", "71", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("345", "71", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("346", "71", "main_sec_2_products_0_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("347", "71", "_main_sec_2_products_0_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("348", "71", "main_sec_2_products_0_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("349", "71", "_main_sec_2_products_0_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("350", "71", "main_sec_2_products_0_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("351", "71", "_main_sec_2_products_0_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("352", "71", "main_sec_2_products_1_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("353", "71", "_main_sec_2_products_1_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("354", "71", "main_sec_2_products_1_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("355", "71", "_main_sec_2_products_1_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("356", "71", "main_sec_2_products_1_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("357", "71", "_main_sec_2_products_1_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("358", "71", "main_sec_2_products_2_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("359", "71", "_main_sec_2_products_2_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("360", "71", "main_sec_2_products_2_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("361", "71", "_main_sec_2_products_2_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("362", "71", "main_sec_2_products_2_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("363", "71", "_main_sec_2_products_2_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("364", "71", "main_sec_2_products_3_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("365", "71", "_main_sec_2_products_3_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("366", "71", "main_sec_2_products_3_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("367", "71", "_main_sec_2_products_3_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("368", "71", "main_sec_2_products_3_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("369", "71", "_main_sec_2_products_3_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("370", "71", "main_sec_2_products_4_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("371", "71", "_main_sec_2_products_4_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("372", "71", "main_sec_2_products_4_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("373", "71", "_main_sec_2_products_4_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("374", "71", "main_sec_2_products_4_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("375", "71", "_main_sec_2_products_4_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("376", "71", "main_sec_2_products", "5");
INSERT INTO `wp_postmeta` VALUES("377", "71", "_main_sec_2_products", "field_5b8d97e92fe3b");
INSERT INTO `wp_postmeta` VALUES("378", "71", "main_sec_2_products_0_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("379", "71", "_main_sec_2_products_0_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("380", "71", "main_sec_2_products_1_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("381", "71", "_main_sec_2_products_1_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("382", "71", "main_sec_2_products_2_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("383", "71", "_main_sec_2_products_2_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("384", "71", "main_sec_2_products_3_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("385", "71", "_main_sec_2_products_3_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("386", "71", "main_sec_2_products_4_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("387", "71", "_main_sec_2_products_4_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("388", "73", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("389", "73", "_edit_lock", "1536011788:1");
INSERT INTO `wp_postmeta` VALUES("391", "2", "main_sec_3_posts", "73");
INSERT INTO `wp_postmeta` VALUES("392", "2", "_main_sec_3_posts", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("393", "75", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("394", "75", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("395", "75", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("396", "75", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("397", "75", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("398", "75", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("399", "75", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("400", "75", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("401", "75", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("402", "75", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("403", "75", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("404", "75", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("405", "75", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("406", "75", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("407", "75", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("408", "75", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("409", "75", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("410", "75", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("411", "75", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("412", "75", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("413", "75", "main_sec_2_products_0_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("414", "75", "_main_sec_2_products_0_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("415", "75", "main_sec_2_products_0_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("416", "75", "_main_sec_2_products_0_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("417", "75", "main_sec_2_products_0_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("418", "75", "_main_sec_2_products_0_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("419", "75", "main_sec_2_products_1_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("420", "75", "_main_sec_2_products_1_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("421", "75", "main_sec_2_products_1_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("422", "75", "_main_sec_2_products_1_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("423", "75", "main_sec_2_products_1_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("424", "75", "_main_sec_2_products_1_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("425", "75", "main_sec_2_products_2_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("426", "75", "_main_sec_2_products_2_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("427", "75", "main_sec_2_products_2_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("428", "75", "_main_sec_2_products_2_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("429", "75", "main_sec_2_products_2_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("430", "75", "_main_sec_2_products_2_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("431", "75", "main_sec_2_products_3_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("432", "75", "_main_sec_2_products_3_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("433", "75", "main_sec_2_products_3_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("434", "75", "_main_sec_2_products_3_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("435", "75", "main_sec_2_products_3_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("436", "75", "_main_sec_2_products_3_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("437", "75", "main_sec_2_products_4_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("438", "75", "_main_sec_2_products_4_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("439", "75", "main_sec_2_products_4_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("440", "75", "_main_sec_2_products_4_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("441", "75", "main_sec_2_products_4_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("442", "75", "_main_sec_2_products_4_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("443", "75", "main_sec_2_products", "5");
INSERT INTO `wp_postmeta` VALUES("444", "75", "_main_sec_2_products", "field_5b8d97e92fe3b");
INSERT INTO `wp_postmeta` VALUES("445", "75", "main_sec_2_products_0_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("446", "75", "_main_sec_2_products_0_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("447", "75", "main_sec_2_products_1_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("448", "75", "_main_sec_2_products_1_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("449", "75", "main_sec_2_products_2_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("450", "75", "_main_sec_2_products_2_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("451", "75", "main_sec_2_products_3_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("452", "75", "_main_sec_2_products_3_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("453", "75", "main_sec_2_products_4_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("454", "75", "_main_sec_2_products_4_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("455", "75", "main_sec_3_posts", "73");
INSERT INTO `wp_postmeta` VALUES("456", "75", "_main_sec_3_posts", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("458", "77", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("460", "77", "_edit_lock", "1536011929:1");
INSERT INTO `wp_postmeta` VALUES("461", "1", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("462", "1", "_wp_trash_meta_time", "1536011947");
INSERT INTO `wp_postmeta` VALUES("463", "1", "_wp_desired_post_slug", "witaj-swiecie");
INSERT INTO `wp_postmeta` VALUES("464", "1", "_wp_trash_meta_comments_status", "a:1:{i:1;s:1:\"1\";}");
INSERT INTO `wp_postmeta` VALUES("466", "2", "main_sec_3_slogan", "Producent innowacyjnych urządzeń dla energetykii przemysłu");
INSERT INTO `wp_postmeta` VALUES("467", "2", "_main_sec_3_slogan", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("468", "83", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("469", "83", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("470", "83", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("471", "83", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("472", "83", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("473", "83", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("474", "83", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("475", "83", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("476", "83", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("477", "83", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("478", "83", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("479", "83", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("480", "83", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("481", "83", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("482", "83", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("483", "83", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("484", "83", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("485", "83", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("486", "83", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("487", "83", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("488", "83", "main_sec_2_products_0_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("489", "83", "_main_sec_2_products_0_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("490", "83", "main_sec_2_products_0_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("491", "83", "_main_sec_2_products_0_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("492", "83", "main_sec_2_products_0_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("493", "83", "_main_sec_2_products_0_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("494", "83", "main_sec_2_products_1_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("495", "83", "_main_sec_2_products_1_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("496", "83", "main_sec_2_products_1_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("497", "83", "_main_sec_2_products_1_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("498", "83", "main_sec_2_products_1_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("499", "83", "_main_sec_2_products_1_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("500", "83", "main_sec_2_products_2_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("501", "83", "_main_sec_2_products_2_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("502", "83", "main_sec_2_products_2_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("503", "83", "_main_sec_2_products_2_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("504", "83", "main_sec_2_products_2_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("505", "83", "_main_sec_2_products_2_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("506", "83", "main_sec_2_products_3_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("507", "83", "_main_sec_2_products_3_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("508", "83", "main_sec_2_products_3_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("509", "83", "_main_sec_2_products_3_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("510", "83", "main_sec_2_products_3_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("511", "83", "_main_sec_2_products_3_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("512", "83", "main_sec_2_products_4_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("513", "83", "_main_sec_2_products_4_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("514", "83", "main_sec_2_products_4_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("515", "83", "_main_sec_2_products_4_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("516", "83", "main_sec_2_products_4_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("517", "83", "_main_sec_2_products_4_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("518", "83", "main_sec_2_products", "5");
INSERT INTO `wp_postmeta` VALUES("519", "83", "_main_sec_2_products", "field_5b8d97e92fe3b");
INSERT INTO `wp_postmeta` VALUES("520", "83", "main_sec_2_products_0_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("521", "83", "_main_sec_2_products_0_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("522", "83", "main_sec_2_products_1_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("523", "83", "_main_sec_2_products_1_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("524", "83", "main_sec_2_products_2_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("525", "83", "_main_sec_2_products_2_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("526", "83", "main_sec_2_products_3_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("527", "83", "_main_sec_2_products_3_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("528", "83", "main_sec_2_products_4_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("529", "83", "_main_sec_2_products_4_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("530", "83", "main_sec_3_posts", "73");
INSERT INTO `wp_postmeta` VALUES("531", "83", "_main_sec_3_posts", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("532", "83", "main_sec_3_slogan", "Producent innowacyjnych urządzeń dla energetykii przemysłu");
INSERT INTO `wp_postmeta` VALUES("533", "83", "_main_sec_3_slogan", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("534", "2", "main_slider_0_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("535", "2", "_main_slider_0_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("536", "2", "main_slider_0_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("537", "2", "_main_slider_0_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("538", "2", "main_slider_1_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("539", "2", "_main_slider_1_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("540", "2", "main_slider_1_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("541", "2", "_main_slider_1_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("542", "2", "main_slider_2_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("543", "2", "_main_slider_2_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("544", "2", "main_slider_2_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("545", "2", "_main_slider_2_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("546", "2", "main_slider_3_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("547", "2", "_main_slider_3_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("548", "2", "main_slider_3_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("549", "2", "_main_slider_3_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("550", "2", "main_slider_4_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("551", "2", "_main_slider_4_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("552", "2", "main_slider_4_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("553", "2", "_main_slider_4_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("554", "2", "main_slider_5_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("555", "2", "_main_slider_5_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("556", "2", "main_slider_5_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("557", "2", "_main_slider_5_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("558", "2", "main_slider", "6");
INSERT INTO `wp_postmeta` VALUES("559", "2", "_main_slider", "field_5b8db02bbbf24");
INSERT INTO `wp_postmeta` VALUES("560", "87", "contact-left-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("561", "87", "_contact-left-col", "field_5b8c29293d507");
INSERT INTO `wp_postmeta` VALUES("562", "87", "contact-right-col", "<strong>Heading</strong>\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11\r\n\r\nul. Przykładowa 11");
INSERT INTO `wp_postmeta` VALUES("563", "87", "_contact-right-col", "field_5b8c296e3d508");
INSERT INTO `wp_postmeta` VALUES("564", "87", "main_slogan_small", "Zasilamy innowacje");
INSERT INTO `wp_postmeta` VALUES("565", "87", "_main_slogan_small", "field_5b8d92ee4bc19");
INSERT INTO `wp_postmeta` VALUES("566", "87", "main_slogan_big", "ENERGIĄ");
INSERT INTO `wp_postmeta` VALUES("567", "87", "_main_slogan_big", "field_5b8d93464bc1a");
INSERT INTO `wp_postmeta` VALUES("568", "87", "main_sec_1_heading_red", "PRODUCENT");
INSERT INTO `wp_postmeta` VALUES("569", "87", "_main_sec_1_heading_red", "field_5b8d94354bc1e");
INSERT INTO `wp_postmeta` VALUES("570", "87", "main_sec_1_heading", "innowacyjnych urządzeń dla energetyki i przemysłu!");
INSERT INTO `wp_postmeta` VALUES("571", "87", "_main_sec_1_heading", "field_5b8d93ac4bc1d");
INSERT INTO `wp_postmeta` VALUES("572", "87", "main_sec_1_btn_red", "");
INSERT INTO `wp_postmeta` VALUES("573", "87", "_main_sec_1_btn_red", "field_5b8d944c4bc1f");
INSERT INTO `wp_postmeta` VALUES("574", "87", "main_sec_1_heading_about", "O nas");
INSERT INTO `wp_postmeta` VALUES("575", "87", "_main_sec_1_heading_about", "field_5b8d94d04bc20");
INSERT INTO `wp_postmeta` VALUES("576", "87", "main_sec_1_text_about", "Naszą misją jest najlepiej rozumieć i zaspokajać potrzeby Klientów, dzięki dopasowanym rozwiązaniom i kilkudziesięcioletniemu doświadczeniu w polskiej branży elektroenergetycznej.");
INSERT INTO `wp_postmeta` VALUES("577", "87", "_main_sec_1_text_about", "field_5b8d955e4bc21");
INSERT INTO `wp_postmeta` VALUES("578", "87", "main_sec_1_btn_more", "");
INSERT INTO `wp_postmeta` VALUES("579", "87", "_main_sec_1_btn_more", "field_5b8d95994bc22");
INSERT INTO `wp_postmeta` VALUES("580", "87", "main_sec_2_products_0_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("581", "87", "_main_sec_2_products_0_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("582", "87", "main_sec_2_products_0_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("583", "87", "_main_sec_2_products_0_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("584", "87", "main_sec_2_products_0_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("585", "87", "_main_sec_2_products_0_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("586", "87", "main_sec_2_products_1_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("587", "87", "_main_sec_2_products_1_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("588", "87", "main_sec_2_products_1_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("589", "87", "_main_sec_2_products_1_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("590", "87", "main_sec_2_products_1_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("591", "87", "_main_sec_2_products_1_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("592", "87", "main_sec_2_products_2_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("593", "87", "_main_sec_2_products_2_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("594", "87", "main_sec_2_products_2_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("595", "87", "_main_sec_2_products_2_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("596", "87", "main_sec_2_products_2_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("597", "87", "_main_sec_2_products_2_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("598", "87", "main_sec_2_products_3_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("599", "87", "_main_sec_2_products_3_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("600", "87", "main_sec_2_products_3_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("601", "87", "_main_sec_2_products_3_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("602", "87", "main_sec_2_products_3_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("603", "87", "_main_sec_2_products_3_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("604", "87", "main_sec_2_products_4_main_sec_2_product_img", "45");
INSERT INTO `wp_postmeta` VALUES("605", "87", "_main_sec_2_products_4_main_sec_2_product_img", "field_5b8d989f2fe3c");
INSERT INTO `wp_postmeta` VALUES("606", "87", "main_sec_2_products_4_main_sec_2_product_link", "");
INSERT INTO `wp_postmeta` VALUES("607", "87", "_main_sec_2_products_4_main_sec_2_product_link", "field_5b8d98dc2fe3d");
INSERT INTO `wp_postmeta` VALUES("608", "87", "main_sec_2_products_4_main_sec_2_product_head\'", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("609", "87", "_main_sec_2_products_4_main_sec_2_product_head\'", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("610", "87", "main_sec_2_products", "5");
INSERT INTO `wp_postmeta` VALUES("611", "87", "_main_sec_2_products", "field_5b8d97e92fe3b");
INSERT INTO `wp_postmeta` VALUES("612", "87", "main_sec_2_products_0_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("613", "87", "_main_sec_2_products_0_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("614", "87", "main_sec_2_products_1_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("615", "87", "_main_sec_2_products_1_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("616", "87", "main_sec_2_products_2_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("617", "87", "_main_sec_2_products_2_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("618", "87", "main_sec_2_products_3_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("619", "87", "_main_sec_2_products_3_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("620", "87", "main_sec_2_products_4_main_sec_2_product_head", "ROZDZIELNICE");
INSERT INTO `wp_postmeta` VALUES("621", "87", "_main_sec_2_products_4_main_sec_2_product_head", "field_5b8d9a082fe3e");
INSERT INTO `wp_postmeta` VALUES("622", "87", "main_sec_3_posts", "73");
INSERT INTO `wp_postmeta` VALUES("623", "87", "_main_sec_3_posts", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("624", "87", "main_sec_3_slogan", "Producent innowacyjnych urządzeń dla energetykii przemysłu");
INSERT INTO `wp_postmeta` VALUES("625", "87", "_main_sec_3_slogan", "field_5b8da34987b72");
INSERT INTO `wp_postmeta` VALUES("626", "87", "main_slider_0_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("627", "87", "_main_slider_0_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("628", "87", "main_slider_0_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("629", "87", "_main_slider_0_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("630", "87", "main_slider_1_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("631", "87", "_main_slider_1_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("632", "87", "main_slider_1_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("633", "87", "_main_slider_1_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("634", "87", "main_slider_2_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("635", "87", "_main_slider_2_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("636", "87", "main_slider_2_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("637", "87", "_main_slider_2_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("638", "87", "main_slider_3_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("639", "87", "_main_slider_3_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("640", "87", "main_slider_3_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("641", "87", "_main_slider_3_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("642", "87", "main_slider_4_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("643", "87", "_main_slider_4_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("644", "87", "main_slider_4_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("645", "87", "_main_slider_4_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("646", "87", "main_slider_5_main_slider_img", "47");
INSERT INTO `wp_postmeta` VALUES("647", "87", "_main_slider_5_main_slider_img", "field_5b8db065bbf25");
INSERT INTO `wp_postmeta` VALUES("648", "87", "main_slider_5_main_slider_alt", "tauron");
INSERT INTO `wp_postmeta` VALUES("649", "87", "_main_slider_5_main_slider_alt", "field_5b8db13abbf26");
INSERT INTO `wp_postmeta` VALUES("650", "87", "main_slider", "6");
INSERT INTO `wp_postmeta` VALUES("651", "87", "_main_slider", "field_5b8db02bbbf24");

/* INSERT TABLE DATA: wp_posts */
INSERT INTO `wp_posts` VALUES("1", "1", "2018-08-28 12:20:05", "2018-08-28 10:20:05", "Witaj w WordPressie. To jest twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij pisanie!", "Witaj, świecie!", "", "trash", "open", "open", "", "witaj-swiecie__trashed", "", "", "2018-09-03 23:59:07", "2018-09-03 21:59:07", "", "0", "http://localhost/wordpress/?p=1", "0", "post", "", "1");
INSERT INTO `wp_posts` VALUES("2", "1", "2018-08-28 12:20:05", "2018-08-28 10:20:05", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "publish", "closed", "open", "", "przykladowa-strona", "", "", "2018-09-04 00:11:58", "2018-09-03 22:11:58", "", "0", "http://localhost/wordpress/?page_id=2", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("3", "1", "2018-08-28 12:20:05", "2018-08-28 10:20:05", "<h2>Kim jesteśmy</h2><p>Adres naszej strony internetowej to: http://localhost/wordpress.</p><h2>Jakie dane osobiste zbieramy i dlaczego je zbieramy</h2><h3>Komentarze</h3><p>Kiedy odwiedzający witrynę zostawia komentarz, zbieramy dane widoczne w formularzu komentowania, jak i adres IP odwiedzającego oraz podpis jego przeglądarki jako pomoc przy wykrywaniu spamu.</p><p>Zanonimizowany ciąg znaków stworzony na podstawie twojego adresu email (tak zwany hash) może zostać przesłany do usługi Gravatar w celu sprawdzenia czy jej używasz. Polityka prywatności usługi Gravatar jest dostępna tutaj: https://automattic.com/privacy/. Po zatwierdzeniu komentarza twój obrazek profilowy jest widoczny publicznie w kontekście twojego komentarza.</p><h3>Media</h3><p>Jeśli jesteś zarejestrowanym użytkownikiem i wgrywasz na witrynę obrazki, powinieneś unikać przesyłania obrazków z tagami EXIF lokalizacji. Odwiedzający stronę mogą pobrać i odczytać pełne dane lokalizacyjne z obrazków w witrynie.</p><h3>Formularze kontaktowe</h3><h3>Ciasteczka</h3><p>Jeśli zostawisz na naszej witrynie komentarz, będziesz mógł wybrać opcję zapisu twojej nazwy, adresu email i adresu strony internetowej w ciasteczkach, dzięki którym podczas pisania kolejnych komentarzy powyższe informacje będą już dogodnie uzupełnione. Te ciasteczka wygasają po roku.</p><p>Jeśli masz konto i zalogujesz się na tej witrynie, utworzymy tymczasowe ciasteczko na potrzeby sprawdzenia czy twoja przeglądarka akceptuje ciasteczka. To ciasteczko nie zawiera żadnych danych osobistych i zostanie wyrzucone kiedy zamkniesz przeglądarkę.</p><p>Podczas logowania tworzymy dodatkowo kilka ciasteczek potrzebnych do zapisu twoich informacji logowania oraz wybranych opcji ekranu. Ciasteczka logowania wygasają po dwóch dniach, a opcji ekranu po roku. Jeśli zaznaczysz opcję &bdquo;Pamiętaj mnie&rdquo;, logowanie wygaśnie po dwóch tygodniach. Jeśli wylogujesz się ze swojego konta, ciasteczka logowania zostaną usunięte.</p><p>Jeśli zmodyfikujesz albo opublikujesz artykuł, w twojej przeglądarce zostanie zapisane dodatkowe ciasteczko. To ciasteczko nie zawiera żadnych danych osobistych, wskazując po prostu na identyfikator przed chwilą edytowanego artykułu. Wygasa ono po 1 dniu.</p><h3>Osadzone treści z innych witryn</h3><p>Artykuły na tej witrynie mogą zawierać osadzone treści (np. filmy, obrazki, artykuły itp.). Osadzone treści z innych witryn zachowują się analogicznie do tego, jakby użytkownik odwiedził bezpośrednio konkretną witrynę.</p><p>Witryny mogą zbierać informacje o tobie, używać ciasteczek, dołączać dodatkowe, zewnętrzne systemy śledzenia i monitorować twoje interakcje z osadzonym materiałem, włączając w to śledzenie twoich interakcji z osadzonym materiałem jeśli posiadasz konto i jesteś zalogowany w tamtej witrynie.</p><h3>Analiza statystyk</h3><h2>Z kim dzielimy się danymi</h2><h2>Jak długo przechowujemy twoje dane</h2><p>Jeśli zostawisz komentarz, jego treść i metadane będą przechowywane przez czas nieokreślony. Dzięki temu jesteśmy w stanie rozpoznawać i zatwierdzać kolejne komentarze automatycznie, bez wysyłania ich do każdorazowej moderacji.</p><p>Dla użytkowników którzy zarejestrowali się na naszej stronie internetowej (jeśli tacy są), przechowujemy również informacje osobiste wprowadzone w profilu. Każdy użytkownik może dokonać wglądu, korekty albo skasować swoje informacje osobiste w dowolnej chwili (nie licząc nazwy użytkownika, której nie można zmienić). Administratorzy strony również mogą przeglądać i modyfikować te informacje.</p><h2>Jakie masz prawa do swoich danych</h2><p>Jeśli masz konto użytkownika albo dodałeś komentarze w tej witrynie, możesz zażądać dostarczenia pliku z wyeksportowanym kompletem twoich danych osobistych będących w naszym posiadaniu, w tym całość tych dostarczonych przez ciebie. Możesz również zażądać usunięcia przez nas całości twoich danych osobistych w naszym posiadaniu. Nie dotyczy to żadnych danych które jesteśmy zobligowani zachować ze względów administracyjnych, prawnych albo bezpieczeństwa.</p><h2>Gdzie przesyłamy dane</h2><p>Komentarze gości mogą być sprawdzane za pomocą automatycznej usługi wykrywania spamu.</p><h2>Twoje dane kontaktowe</h2><h2>Informacje dodatkowe</h2><h3>Jak chronimy twoje dane?</h3><h3>Jakie mamy obowiązujące procedury w przypadku naruszenia prywatności danych</h3><h3>Od jakich stron trzecich otrzymujemy dane</h3><h3>Jakie automatyczne podejmowanie decyzji i/lub tworzenie profili przeprowadzamy z użyciem danych użytkownika</h3><h3>Branżowe wymogi regulacyjne dotyczące ujawniania informacji</h3>", "Polityka prywatności", "", "draft", "closed", "open", "", "polityka-prywatnosci", "", "", "2018-08-28 12:20:05", "2018-08-28 10:20:05", "", "0", "http://localhost/wordpress/?page_id=3", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("4", "1", "2018-08-28 12:21:01", "0000-00-00 00:00:00", "", "Automatycznie zapisany szkic", "", "auto-draft", "open", "open", "", "", "", "", "2018-08-28 12:21:01", "0000-00-00 00:00:00", "", "0", "http://localhost/wordpress/?p=4", "0", "post", "", "0");
INSERT INTO `wp_posts` VALUES("6", "1", "2018-08-28 16:29:01", "2018-08-28 14:29:01", "", "Realizacje", "", "publish", "closed", "closed", "", "6", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=6", "1", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("7", "1", "2018-08-28 23:28:58", "2018-08-28 21:28:58", "", "Warstwa 2", "", "inherit", "open", "closed", "", "warstwa-2", "", "", "2018-08-28 23:28:58", "2018-08-28 21:28:58", "", "0", "http://localhost/wordpress/wp-content/uploads/2018/08/Warstwa-2.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("8", "1", "2018-08-29 12:52:13", "2018-08-29 10:52:13", "", "logo-white", "", "inherit", "open", "closed", "", "logo-white", "", "", "2018-08-29 12:52:13", "2018-08-29 10:52:13", "", "0", "http://localhost/wordpress/wp-content/uploads/2018/08/logo-white.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("18", "1", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:18:\"acf-options-stopka\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Menu stopka", "menu-stopka", "publish", "closed", "closed", "", "group_5b880039df3fb", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "0", "http://localhost/wordpress/?post_type=acf-field-group&#038;p=18", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("19", "1", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Linki menu (pierwsza kolumna)", "linki_menu_(pierwsza_kolumna)", "publish", "closed", "closed", "", "field_5b88007a89228", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&#038;p=19", "4", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("20", "1", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:34:\"Wybierz linki do pierwszej kolumny\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}", "Linki menu (pierwsza kolumna)", "footer_menu_links_1", "publish", "closed", "closed", "", "field_5b88005089227", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&#038;p=20", "5", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("21", "1", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "a:10:{s:4:\"type\";s:9:\"page_link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:0:{}s:8:\"taxonomy\";a:0:{}s:10:\"allow_null\";i:0;s:14:\"allow_archives\";i:1;s:8:\"multiple\";i:0;}", "Link", "footer_menu_link", "publish", "closed", "closed", "", "field_5b8800bc89229", "", "", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "", "20", "http://localhost/wordpress/?post_type=acf-field&p=21", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("22", "1", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Tekst linku", "footer_menu_link_desc", "publish", "closed", "closed", "", "field_5b8800f68922a", "", "", "2018-08-30 16:37:18", "2018-08-30 14:37:18", "", "20", "http://localhost/wordpress/?post_type=acf-field&p=22", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("23", "1", "2018-08-31 11:10:08", "2018-08-31 09:10:08", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Linki menu (druga kolumna)", "_kopia", "publish", "closed", "closed", "", "field_5b8905be3685b", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&#038;p=23", "6", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("24", "1", "2018-08-31 11:10:08", "2018-08-31 09:10:08", "a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:34:\"Wybierz linki do pierwszej kolumny\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}", "Linki menu (druga kolumna)", "footer_menu_links_2", "publish", "closed", "closed", "", "field_5b8905d43685c", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&#038;p=24", "7", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("25", "1", "2018-08-31 11:10:08", "2018-08-31 09:10:08", "a:10:{s:4:\"type\";s:9:\"page_link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:0:{}s:8:\"taxonomy\";a:0:{}s:10:\"allow_null\";i:0;s:14:\"allow_archives\";i:1;s:8:\"multiple\";i:0;}", "Link", "footer_menu_link", "publish", "closed", "closed", "", "field_5b8905d43685d", "", "", "2018-08-31 11:10:08", "2018-08-31 09:10:08", "", "24", "http://localhost/wordpress/?post_type=acf-field&p=25", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("26", "1", "2018-08-31 11:10:08", "2018-08-31 09:10:08", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Tekst linku", "footer_menu_link_desc", "publish", "closed", "closed", "", "field_5b8905d43685e", "", "", "2018-08-31 11:10:08", "2018-08-31 09:10:08", "", "24", "http://localhost/wordpress/?post_type=acf-field&p=26", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("27", "1", "2018-08-31 11:12:18", "2018-08-31 09:12:18", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Dane adresowe", "dane_adresowe", "publish", "closed", "closed", "", "field_5b89065e879e3", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&#038;p=27", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("28", "1", "2018-08-31 11:12:18", "2018-08-31 09:12:18", "a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:19:\"Wpisz dane adresowe\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}", "Adres", "footer_addres", "publish", "closed", "closed", "", "field_5b890615879e2", "", "", "2018-08-31 11:12:18", "2018-08-31 09:12:18", "", "18", "http://localhost/wordpress/?post_type=acf-field&p=28", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("29", "1", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Dane kontaktowe", "dane_kontaktowe", "publish", "closed", "closed", "", "field_5b8907a7ff044", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&p=29", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("30", "1", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:19:\"Wpisz dane adresowe\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}", "Kontakt", "footer_contact", "publish", "closed", "closed", "", "field_5b8907ccff045", "", "", "2018-08-31 11:18:21", "2018-08-31 09:18:21", "", "18", "http://localhost/wordpress/?post_type=acf-field&p=30", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("31", "1", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Ogólne opcje motywu", "ogolne-opcje-motywu", "publish", "closed", "closed", "", "group_5b890a4fa0690", "", "", "2018-09-01 16:35:58", "2018-09-01 14:35:58", "", "0", "http://localhost/wordpress/?post_type=acf-field-group&#038;p=31", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("32", "1", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Ikony social media", "social_media", "publish", "closed", "closed", "", "field_5b890a6ac349f", "", "", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "", "31", "http://localhost/wordpress/?post_type=acf-field&p=32", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("33", "1", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:10:\"Wklej link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Facebook", "social_facebook", "publish", "closed", "closed", "", "field_5b890a7bc34a0", "", "", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "", "31", "http://localhost/wordpress/?post_type=acf-field&p=33", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("34", "1", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:10:\"Wklej link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Google plus", "social_google", "publish", "closed", "closed", "", "field_5b890ac4c34a1", "", "", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "", "31", "http://localhost/wordpress/?post_type=acf-field&p=34", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("35", "1", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:10:\"Wklej link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Twitter", "social_twitter", "publish", "closed", "closed", "", "field_5b890adac34a2", "", "", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "", "31", "http://localhost/wordpress/?post_type=acf-field&p=35", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("36", "1", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:10:\"Wklej link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Youtube", "social_youtube", "publish", "closed", "closed", "", "field_5b890aeac34a3", "", "", "2018-08-31 11:31:40", "2018-08-31 09:31:40", "", "31", "http://localhost/wordpress/?post_type=acf-field&p=36", "4", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("37", "1", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "Realizacje", "", "publish", "closed", "closed", "", "realizacje", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=37", "2", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("38", "1", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "Realizacje", "", "publish", "closed", "closed", "", "realizacje-2", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=38", "3", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("39", "1", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "Realizacje", "", "publish", "closed", "closed", "", "realizacje-3", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=39", "4", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("40", "1", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "Realizacje", "", "publish", "closed", "closed", "", "realizacje-4", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=40", "5", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("41", "1", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "Realizacje", "", "publish", "closed", "closed", "", "realizacje-5", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=41", "6", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("42", "1", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "Realizacje", "", "publish", "closed", "closed", "", "realizacje-6", "", "", "2018-08-31 12:40:38", "2018-08-31 10:40:38", "", "0", "http://localhost/wordpress/?p=42", "7", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("43", "1", "2018-08-31 13:32:36", "2018-08-31 11:32:36", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-08-31 13:32:36", "2018-08-31 11:32:36", "", "2", "http://localhost/wordpress/2018/08/31/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("44", "1", "2018-08-31 13:34:48", "2018-08-31 11:34:48", "", "main_page_bg", "", "inherit", "open", "closed", "", "main_page_bg", "", "", "2018-08-31 13:34:48", "2018-08-31 11:34:48", "", "2", "http://localhost/wordpress/wp-content/uploads/2018/08/main_page_bg.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("45", "1", "2018-08-31 22:01:23", "2018-08-31 20:01:23", "", "Warstwa 32product", "", "inherit", "open", "closed", "", "warstwa-32product", "", "", "2018-09-03 22:33:19", "2018-09-03 20:33:19", "", "2", "http://localhost/wordpress/wp-content/uploads/2018/08/Warstwa-32product.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("46", "1", "2018-08-31 22:33:12", "2018-08-31 20:33:12", "", "main_bg_2", "", "inherit", "open", "closed", "", "main_bg_2", "", "", "2018-08-31 22:33:12", "2018-08-31 20:33:12", "", "0", "http://localhost/wordpress/wp-content/uploads/2018/08/main_bg_2.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("47", "1", "2018-09-01 00:50:05", "2018-08-31 22:50:05", "", "Warstwa 19brand", "", "inherit", "open", "closed", "", "warstwa-19brand", "", "", "2018-09-04 00:11:58", "2018-09-03 22:11:58", "", "2", "http://localhost/wordpress/wp-content/uploads/2018/09/Warstwa-19brand.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("48", "1", "2018-09-01 16:35:58", "2018-09-01 14:35:58", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Google maps - klucz API", "maps_api_key", "publish", "closed", "closed", "", "field_5b8aa379e6a24", "", "", "2018-09-01 16:35:58", "2018-09-01 14:35:58", "", "31", "http://localhost/wordpress/?post_type=acf-field&p=48", "5", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("49", "1", "2018-09-02 20:18:43", "2018-09-02 18:18:43", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"page-main.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:1:{i:0;s:11:\"the_content\";}s:11:\"description\";s:0:\"\";}", "Strona główna", "strona-glowna", "publish", "closed", "closed", "", "group_5b8c291741723", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "0", "http://localhost/wordpress/?post_type=acf-field-group&#038;p=49", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("50", "1", "2018-09-02 20:18:43", "2018-09-02 18:18:43", "a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}", "Dane kontaktowe - kolumna lewa", "contact-left-col", "publish", "closed", "closed", "", "field_5b8c29293d507", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=50", "17", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("51", "1", "2018-09-02 20:18:43", "2018-09-02 18:18:43", "a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}", "Dane kontaktowe - kolumna prawa", "contact-right-col", "publish", "closed", "closed", "", "field_5b8c296e3d508", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=51", "18", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("52", "1", "2018-09-02 20:20:08", "2018-09-02 18:20:08", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-02 20:20:08", "2018-09-02 18:20:08", "", "2", "http://localhost/wordpress/2018/09/02/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("53", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Główny slogan", "glowny_slogan", "publish", "closed", "closed", "", "field_5b8d935c4bc1b", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=53", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("54", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:23:\"Tekst w czerwonym dymku\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Główny slogan - mały napis", "main_slogan_small", "publish", "closed", "closed", "", "field_5b8d92ee4bc19", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=54", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("55", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:23:\"Tekst w czerwonym dymku\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Główny slogan - duży napis", "main_slogan_big", "publish", "closed", "closed", "", "field_5b8d93464bc1a", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=55", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("56", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Pierwsza sekcja", "pierwsza_sekcja", "publish", "closed", "closed", "", "field_5b8d939f4bc1c", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=56", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("57", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Nagłówek - czerwony tekst", "main_sec_1_heading_red", "publish", "closed", "closed", "", "field_5b8d94354bc1e", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=57", "4", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("58", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Nagłówek - szary tekst", "main_sec_1_heading", "publish", "closed", "closed", "", "field_5b8d93ac4bc1d", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=58", "5", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("59", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";}", "Przycisk \"napisz do nas\"", "main_sec_1_btn_red", "publish", "closed", "closed", "", "field_5b8d944c4bc1f", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=59", "6", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("60", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Nagłówek \"o nas\"", "main_sec_1_heading_about", "publish", "closed", "closed", "", "field_5b8d94d04bc20", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=60", "7", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("61", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}", "Tekst \"o nas\"", "main_sec_1_text_about", "publish", "closed", "closed", "", "field_5b8d955e4bc21", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=61", "8", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("62", "1", "2018-09-03 22:12:57", "2018-09-03 20:12:57", "a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";}", "Przycisk \"czytaj więcej\"", "main_sec_1_btn_more", "publish", "closed", "closed", "", "field_5b8d95994bc22", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=62", "9", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("63", "1", "2018-09-03 22:14:58", "2018-09-03 20:14:58", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-03 22:14:58", "2018-09-03 20:14:58", "", "2", "http://localhost/wordpress/2018/09/03/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("64", "1", "2018-09-03 22:19:14", "2018-09-03 20:19:14", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-03 22:19:14", "2018-09-03 20:19:14", "", "2", "http://localhost/wordpress/2018/09/03/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("65", "1", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Produkty - kategorie", "produkty_-_kategorie", "publish", "closed", "closed", "", "field_5b8d97d62fe3a", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=65", "10", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("66", "1", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}", "Produkt", "main_sec_2_products", "publish", "closed", "closed", "", "field_5b8d97e92fe3b", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=66", "11", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("67", "1", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Obrazek kategorii", "main_sec_2_product_img", "publish", "closed", "closed", "", "field_5b8d989f2fe3c", "", "", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "", "66", "http://localhost/wordpress/?post_type=acf-field&p=67", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("68", "1", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}", "Link kategorii", "main_sec_2_product_link", "publish", "closed", "closed", "", "field_5b8d98dc2fe3d", "", "", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "", "66", "http://localhost/wordpress/?post_type=acf-field&p=68", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("69", "1", "2018-09-03 22:31:49", "2018-09-03 20:31:49", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Nagłówek kategorii", "main_sec_2_product_head", "publish", "closed", "closed", "", "field_5b8d9a082fe3e", "", "", "2018-09-03 22:34:36", "2018-09-03 20:34:36", "", "66", "http://localhost/wordpress/?post_type=acf-field&#038;p=69", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("70", "1", "2018-09-03 22:33:19", "2018-09-03 20:33:19", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-03 22:33:19", "2018-09-03 20:33:19", "", "2", "http://localhost/wordpress/2018/09/03/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("71", "1", "2018-09-03 22:36:04", "2018-09-03 20:36:04", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-03 22:36:04", "2018-09-03 20:36:04", "", "2", "http://localhost/wordpress/2018/09/03/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("72", "1", "2018-09-03 23:12:57", "2018-09-03 21:12:57", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Slogan na kafelku", "main_sec_3_slogan", "publish", "closed", "closed", "", "field_5b8da34987b72", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=72", "13", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("73", "1", "2018-09-03 23:13:27", "2018-09-03 21:13:27", "Lorem ipsum dolor sit amet, an sale noster deleniti cum. Eirmod principes scribentur ut nam. Eos porro fabellas principes ne, illum elitr convenire mea ex, corpora adolescens ei mel. Ut essent ceteros vis, ex qui euismod ceteros. Diam utamur ad eam.", "Artykuł nr. 1", "", "publish", "open", "open", "", "terst", "", "", "2018-09-03 23:56:53", "2018-09-03 21:56:53", "", "0", "http://localhost/wordpress/?p=73", "0", "post", "", "0");
INSERT INTO `wp_posts` VALUES("74", "1", "2018-09-03 23:13:27", "2018-09-03 21:13:27", "Test posta", "Terst", "", "inherit", "closed", "closed", "", "73-revision-v1", "", "", "2018-09-03 23:13:27", "2018-09-03 21:13:27", "", "73", "http://localhost/wordpress/2018/09/03/73-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("75", "1", "2018-09-03 23:13:45", "2018-09-03 21:13:45", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-03 23:13:45", "2018-09-03 21:13:45", "", "2", "http://localhost/wordpress/2018/09/03/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("76", "1", "2018-09-03 23:56:53", "2018-09-03 21:56:53", "Lorem ipsum dolor sit amet, an sale noster deleniti cum. Eirmod principes scribentur ut nam. Eos porro fabellas principes ne, illum elitr convenire mea ex, corpora adolescens ei mel. Ut essent ceteros vis, ex qui euismod ceteros. Diam utamur ad eam.", "Artykuł nr. 1", "", "inherit", "closed", "closed", "", "73-revision-v1", "", "", "2018-09-03 23:56:53", "2018-09-03 21:56:53", "", "73", "http://localhost/wordpress/2018/09/03/73-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("77", "1", "2018-09-03 23:59:03", "2018-09-03 21:59:03", "Lorem ipsum dolor sit amet, an sale noster deleniti cum. Eirmod principes scribentur ut nam. Eos porro fabellas principes ne, illum elitr convenire mea ex, corpora adolescens ei mel. Ut essent ceteros vis, ex qui euismod ceteros. Diam utamur ad eam.", "Artykuł nr. 2", "", "publish", "open", "open", "", "artykul-nr-2", "", "", "2018-09-03 23:59:28", "2018-09-03 21:59:28", "", "0", "http://localhost/wordpress/?p=77", "0", "post", "", "0");
INSERT INTO `wp_posts` VALUES("78", "1", "2018-09-03 23:59:03", "2018-09-03 21:59:03", "Lorem ipsum dolor sit amet, an sale noster deleniti cum. Eirmod principes scribentur ut nam. Eos porro fabellas principes ne, illum elitr convenire mea ex, corpora adolescens ei mel. Ut essent ceteros vis, ex qui euismod ceteros. Diam utamur ad eam.", "Artykuł nr.2", "", "inherit", "closed", "closed", "", "77-revision-v1", "", "", "2018-09-03 23:59:03", "2018-09-03 21:59:03", "", "77", "http://localhost/wordpress/2018/09/03/77-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("79", "1", "2018-09-03 23:59:07", "2018-09-03 21:59:07", "Witaj w WordPressie. To jest twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij pisanie!", "Witaj, świecie!", "", "inherit", "closed", "closed", "", "1-revision-v1", "", "", "2018-09-03 23:59:07", "2018-09-03 21:59:07", "", "1", "http://localhost/wordpress/2018/09/03/1-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("80", "1", "2018-09-03 23:59:28", "2018-09-03 21:59:28", "Lorem ipsum dolor sit amet, an sale noster deleniti cum. Eirmod principes scribentur ut nam. Eos porro fabellas principes ne, illum elitr convenire mea ex, corpora adolescens ei mel. Ut essent ceteros vis, ex qui euismod ceteros. Diam utamur ad eam.", "Artykuł nr. 2", "", "inherit", "closed", "closed", "", "77-revision-v1", "", "", "2018-09-03 23:59:28", "2018-09-03 21:59:28", "", "77", "http://localhost/wordpress/2018/09/03/77-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("81", "1", "2018-09-04 00:02:38", "2018-09-03 22:02:38", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Kafelki", "kafelki", "publish", "closed", "closed", "", "field_5b8da4dd98e98", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=81", "12", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("82", "1", "2018-09-04 00:02:38", "2018-09-03 22:02:38", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Slider", "slider", "publish", "closed", "closed", "", "field_5b8da9b998e99", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=82", "14", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("83", "1", "2018-09-04 00:02:55", "2018-09-03 22:02:55", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-04 00:02:55", "2018-09-03 22:02:55", "", "2", "http://localhost/wordpress/2018/09/04/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("84", "1", "2018-09-04 00:10:51", "2018-09-03 22:10:51", "a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}", "Slider", "main_slider", "publish", "closed", "closed", "", "field_5b8db02bbbf24", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&#038;p=84", "15", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("85", "1", "2018-09-04 00:10:51", "2018-09-03 22:10:51", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Logo", "main_slider_img", "publish", "closed", "closed", "", "field_5b8db065bbf25", "", "", "2018-09-04 00:10:51", "2018-09-03 22:10:51", "", "84", "http://localhost/wordpress/?post_type=acf-field&p=85", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("86", "1", "2018-09-04 00:10:51", "2018-09-03 22:10:51", "a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}", "Opis alternatywny (alt)", "main_slider_alt", "publish", "closed", "closed", "", "field_5b8db13abbf26", "", "", "2018-09-04 00:10:51", "2018-09-03 22:10:51", "", "84", "http://localhost/wordpress/?post_type=acf-field&p=86", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("87", "1", "2018-09-04 00:11:58", "2018-09-03 22:11:58", "To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\r\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię piña coladę (a także spacery, gdy pada deszcz).</blockquote>\r\n... lub taki:\r\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\r\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/wordpress/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!", "Przykładowa strona", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2018-09-04 00:11:58", "2018-09-03 22:11:58", "", "2", "http://localhost/wordpress/2018/09/04/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("88", "1", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}", "Sekcja kontakt", "sekcja_kontakt", "publish", "closed", "closed", "", "field_5b8db1c46f5e5", "", "", "2018-09-04 00:12:58", "2018-09-03 22:12:58", "", "49", "http://localhost/wordpress/?post_type=acf-field&p=88", "16", "acf-field", "", "0");

/* INSERT TABLE DATA: wp_term_relationships */
INSERT INTO `wp_term_relationships` VALUES("1", "1", "0");
INSERT INTO `wp_term_relationships` VALUES("6", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("37", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("38", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("39", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("40", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("41", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("42", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("73", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("77", "3", "0");

/* INSERT TABLE DATA: wp_term_taxonomy */
INSERT INTO `wp_term_taxonomy` VALUES("1", "1", "category", "", "0", "0");
INSERT INTO `wp_term_taxonomy` VALUES("2", "2", "nav_menu", "", "0", "7");
INSERT INTO `wp_term_taxonomy` VALUES("3", "3", "category", "", "0", "2");

/* INSERT TABLE DATA: wp_terms */
INSERT INTO `wp_terms` VALUES("1", "Bez kategorii", "bez-kategorii", "0");
INSERT INTO `wp_terms` VALUES("2", "Menu główne", "menu-glowne", "0");
INSERT INTO `wp_terms` VALUES("3", "Aktualności", "news_posts", "0");

/* INSERT TABLE DATA: wp_usermeta */
INSERT INTO `wp_usermeta` VALUES("1", "1", "nickname", "admin");
INSERT INTO `wp_usermeta` VALUES("2", "1", "first_name", "");
INSERT INTO `wp_usermeta` VALUES("3", "1", "last_name", "");
INSERT INTO `wp_usermeta` VALUES("4", "1", "description", "");
INSERT INTO `wp_usermeta` VALUES("5", "1", "rich_editing", "true");
INSERT INTO `wp_usermeta` VALUES("6", "1", "syntax_highlighting", "true");
INSERT INTO `wp_usermeta` VALUES("7", "1", "comment_shortcuts", "false");
INSERT INTO `wp_usermeta` VALUES("8", "1", "admin_color", "fresh");
INSERT INTO `wp_usermeta` VALUES("9", "1", "use_ssl", "0");
INSERT INTO `wp_usermeta` VALUES("10", "1", "show_admin_bar_front", "true");
INSERT INTO `wp_usermeta` VALUES("11", "1", "locale", "");
INSERT INTO `wp_usermeta` VALUES("12", "1", "wp_capabilities", "a:1:{s:13:\"administrator\";b:1;}");
INSERT INTO `wp_usermeta` VALUES("13", "1", "wp_user_level", "10");
INSERT INTO `wp_usermeta` VALUES("14", "1", "dismissed_wp_pointers", "wp496_privacy");
INSERT INTO `wp_usermeta` VALUES("15", "1", "show_welcome_panel", "1");
INSERT INTO `wp_usermeta` VALUES("16", "1", "session_tokens", "a:1:{s:64:\"1286d6ea57412a5c0ee33d0fa102267010dc826a39a002de5a65457a782dffb7\";a:4:{s:10:\"expiration\";i:1536072011;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535899211;}}");
INSERT INTO `wp_usermeta` VALUES("17", "1", "wp_dashboard_quick_press_last_post_id", "4");
INSERT INTO `wp_usermeta` VALUES("18", "1", "managenav-menuscolumnshidden", "a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}");
INSERT INTO `wp_usermeta` VALUES("19", "1", "metaboxhidden_nav-menus", "a:1:{i:0;s:12:\"add-post_tag\";}");
INSERT INTO `wp_usermeta` VALUES("20", "1", "nav_menu_recently_edited", "2");
INSERT INTO `wp_usermeta` VALUES("21", "1", "wp_user-settings", "libraryContent=browse");
INSERT INTO `wp_usermeta` VALUES("22", "1", "wp_user-settings-time", "1535745663");

/* INSERT TABLE DATA: wp_users */
INSERT INTO `wp_users` VALUES("1", "admin", "$P$BbXTRR2fMHu4tB5Nuau6ZUInyg0sdm/", "admin", "kamil@studiograficzne.com", "", "2018-08-28 10:20:05", "", "0", "admin");

SET FOREIGN_KEY_CHECKS = 1; 

/* Duplicator WordPress Timestamp: 2018-09-03 22:18:03*/
/* DUPLICATOR_MYSQLDUMP_EOF */
