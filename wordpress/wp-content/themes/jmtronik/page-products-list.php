<?php /* Template Name: Lista produktów */ ?>

<?php get_header(); ?>

<div class="container breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="offset-md-4 col-md-8 offset-lg-3 col-lg-9">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h2>
        </div>
    </div>
</div>

<section class="container about">
    <div class="row">
        <div class="col-md-4 col-lg-3 about__nav">
            <ul class="nav nav-tabs about__nav-ul" id="nav-tab" role="tablist">
                <?php
                if( have_rows('products_tabs', 'option') ):
                    while ( have_rows('products_tabs', 'option') ) : the_row(); ?>
                        <li class="nav-item about__nav-li">
                            <a class="nav-link about__nav-a"
                            href="<?php the_sub_field('products_link'); ?>">
                                <?php the_sub_field('products_button'); ?>
                                <i class="fas fa-caret-right"></i>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>

        <div class="col-md-8 col-lg-9 about__content">
            <?php
            if( have_rows('product') ):
                while ( have_rows('product') ) : the_row();
                ?>
                <div class="col-sm-12 col-md-6 col-xl-4 products__product-spacer">
                    <div class="products__product-wrapper">
                        <div class="products__img-wrapper">
                            <img src="<?php the_sub_field('product_img'); ?>" alt="">
                        </div>
                        <div class="products__text-wrapper">
                            <a href="<?php the_sub_field('product_link'); ?>">
                                <h4><?php the_sub_field('product_title'); ?></h4>
                                <p><?php $product_text_split = get_sub_field('product_text');
                                echo substr($product_text_split, 0, 35);?> ...</p>
                                <p>więcej ></p>
                            </a>
                        </div>
                    </div>
                </div>


                <?php endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
<section class="container-fluid products-desc mr-0 ml-auto">
    <div class="row products-desc__text-wrapper">
        <?php the_field('products_description'); ?>
    </div>

</section>
<?php get_template_part( 'template-parts/full-width-img' ) ?>

<?php get_footer(); ?>