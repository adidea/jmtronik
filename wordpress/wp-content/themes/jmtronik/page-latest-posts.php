<?php /* Template Name: Aktualności */ ?>

<?php get_header(); ?>

<section class="container-fluid latest">
    <div class="latest__breadcrumbs">
        <?php if(function_exists('bcn_display'))
        {
            bcn_display();
        }?>
    </div>
    <h2 style="my-3"><?php the_title(); ?></h2>
    <div class="latest__masonry">
        <?php $catquery = new WP_Query( 'cat=3&posts_per_page=-1' ); ?>
        <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
            <div class="latest__masonry-item">
                <div class="latest__masonry-content">
                    <div class="latest__masonry--img-wrapper">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="latest__masonry--text-wrapper">
                        <a href="<?php the_permalink() ?>">
                            <h5><?php the_title(); ?></h5>
                            <p>
                                <span class="post-info--light-gray">
                                    <?php the_time(get_option('date_format')); ?> | by
                                </span>
                                <span class="post-info--dark-gray">
                                    <?php the_author(); ?>
                                </span>
                            </p>
                            <p>
                                <?php the_excerpt(); ?>
                            </p>
                            <p class="latest__masonry--read-more">CZYTAJ WIĘCEJ  >><p>
                        </a>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    </div>
</section>

<?php get_footer(); ?>
