<?php /* Template Name: Kontakt */ ?>

<?php get_header(); ?>

<div class="container" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="col contact-p__breadcrumbs">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h2>
        </div>
    </div>
</div>

<section class="container contact-p">
    <div class="row">
        <div class="col-lg-4">
            <h2 class="heading--red mb-4 mt-2">
                Napisz do nas
            </h2>
            <?php echo do_shortcode('[contact-form-7 id="473" title="Strona kontakt"]'); ?>
        </div>
        <div class="col-lg-8">
            <div class="row">
                <div class="col">
                    <iframe src="https://snazzymaps.com/embed/95367" width="100%" height="400px" style="border:none;"></iframe>
                </div>
            </div>
            <div class="row p-4 mt-5">
                <div class="col-md-4">
                    <img style="footer__logo" src="<?php echo get_template_directory_uri(); ?>/img/Warstwa-2.png" alt="jm tronik logo" class="footer__logo">
                </div>
                <?php
                if( have_rows('contact_p') ):
                    while ( have_rows('contact_p') ) : the_row();
                    ?>
                        <div class="col-md-4 contact-p__wysiwyg mt-3">
                            <?php the_sub_field('contact_p_wysiwyg'); ?>
                        </div>

                    <?php endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>