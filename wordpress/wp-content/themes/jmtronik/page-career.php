<?php /* Template Name: Kariera */ ?>

<?php get_header(); ?>

<div class="container breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="offset-md-4 col-md-8 offset-lg-3 col-lg-9">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h2>
        </div>
    </div>
</div>

<section class="container about">
    <div class="row">
        <div class="col-md-4 col-lg-3 about__nav">
            <ul class="nav nav-tabs about__nav-ul" id="nav-tab" role="tablist">
                <?php
                if( have_rows('career_tabs', 'option') ):
                    while ( have_rows('career_tabs', 'option') ) : the_row(); ?>
                        <li class="nav-item about__nav-li">
                            <a class="nav-link about__nav-a"
                            href="<?php the_sub_field('career_link'); ?>">
                                <?php the_sub_field('career_button'); ?>
                                <i class="fas fa-caret-right"></i>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>

        <div class="col-md-8 col-lg-9 about__content">
            <?php if( get_field('career_desc_toogle') ): ?>
                <div class="career__wide-photo">
                    <?php $career_img = get_field('career_photo'); ?>
                    <img src="<?php echo $career_img['url']; ?>" alt="<?php echo $career_img['alt']; ?>" />
                </div>
                <div class="career__wide-text">
                    <?php the_field('career_text'); ?>
                </div>

            <?php endif; ?>
            <div id="accordion">
            <?php
            $count_tab = 0;
            $count_aria = 0;
                if( have_rows('accord_career') ):
                    while ( have_rows('accord_career') ) : the_row();
                    ?>
                <div class="card help__card career__card">
                    <div class="card-header help__card-header career__card-header" id="<?php the_sub_field('accord_id'); ?>">
                        <a class="btn btn-link help__accord-btn <?php if ($count_aria != 0) echo 'collapsed'; ?>" data-toggle="collapse" data-target="#ctrl<?php the_sub_field('accord_id'); ?>" <?php if ($count_aria == 0) echo 'aria-expanded="true"'; ?> aria-controls="ctrl<?php the_sub_field('accord_id'); ?>">
                            <?php the_sub_field('accord_btn'); ?>
                        </a>
                    </div>

                    <div id="ctrl<?php the_sub_field('accord_id'); ?>" class="collapse <?php if ($count_tab == 0) echo 'show'; ?>" aria-labelledby="<?php the_sub_field('accord_id'); ?>" data-parent="#accordion">
                        <div class="card-body help__card-body career__accord-body">
                            <?php the_sub_field('accord_text'); ?>
                        </div>
                    </div>
                </div>
            <?php
            $count_tab++;
            $count_aria++;
            endwhile;
            endif;
            ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
