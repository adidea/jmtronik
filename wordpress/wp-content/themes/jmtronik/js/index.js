$(document).ready(function(){
  var owl = $('.owl-carousel');
  owl.owlCarousel({
      items:5,
      loop:true,
      margin:20,
      autoplay:true,
      autoplayTimeout:3500,
      autoplayHoverPause:true,
      responsive:{
        0:{
          items:2,
        },
        600:{
            items:3,
        },
        1000:{
            items:5,
        }
      }
  });

  if ( window.location.pathname == '/' ) $('.header-menu').addClass('dark-nav');

  //slick slider

  $(document).ready(function(){
    $('.slider-single').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: false,
      adaptiveHeight: false,
      infinite: false,
     useTransform: true,
      speed: 400,
      cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
    });

    $('.slider-nav')
      .on('init', function(event, slick) {
        $('.slider-nav .slick-slide.slick-current').addClass('is-active');
      })
      .slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        arrows: true,
        focusOnSelect: false,
        infinite: false,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        }, {
          breakpoint: 640,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
         }
        }, {
          breakpoint: 420,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
       }
        }]
      });

    $('.slider-single').on('afterChange', function(event, slick, currentSlide) {
      $('.slider-nav').slick('slickGoTo', currentSlide);
      var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
      $('.slider-nav .slick-slide.is-active').removeClass('is-active');
      $(currrentNavSlideElem).addClass('is-active');
    });

    $('.slider-nav').on('click', '.slick-slide', function(event) {
      event.preventDefault();
      var goToSingleSlide = $(this).data('slick-index');

      $('.slider-single').slick('slickGoTo', goToSingleSlide);
    });
  });

  //masonry

  function resizeGridItem(item){
    grid = document.getElementsByClassName("latest__masonry")[0];
    rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
    rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
    rowSpan = Math.ceil((item.querySelector('.latest__masonry-content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
    item.style.gridRowEnd = "span "+rowSpan;
 }

 function resizeAllGridItems(){
    allItems = document.getElementsByClassName("latest__masonry-item");
    for(x=0;x<allItems.length;x++){
       resizeGridItem(allItems[x]);
    }
 }

 window.onload = resizeAllGridItems();

 window.addEventListener("resize", resizeAllGridItems);

 allItems = document.getElementsByClassName("latest__masonry-item");
 for(x=0;x<allItems.length;x++){
    imagesLoaded( allItems[x], resizeInstance);
 }

 function resizeInstance(instance){
    item = instance.elements[0];
    resizeGridItem(item);
 }

 for (var i = 0; i < document.links.length; i++) {
  if (document.links[i].href == document.URL) {
      document.links[i].classList.add("activeLink");
  }
}

console.log('jsworking');
  });



