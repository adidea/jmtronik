<section class="container-fluid contact__container">
    <div class="row">
        <div class="col-12 col-lg-6 col-xl-7 contact__map-wrapper">
            <iframe src="https://snazzymaps.com/embed/95367" width="100%" height="600px" style="border:none;"></iframe>
        </div>
        <div class="col-12 col-lg-6 col-xl-5 contact__data">
            <div class="row">
                <div class="contact__data-heading ml-3">
                    <h4><span class="heading--red">Skontaktuj się</span> z nami</h4>
                </div>
            </div>
            <div class="row contact__info-wrapper">
                <div class="col-6 col-md-5 contact-p__wysiwyg mt-3 contact__data-info">
                    <?php the_field('contact-left-col') ?>
                </div>
                <div class="col-6 col-md-4 contact-p__wysiwyg mt-3 contact__data-info">
                    <?php the_field('contact-right-col') ?>
                </div>
            </div>
            <div class="row contact__btn-wrapper">
                <a href="" class="btn-std mb-5">Napisz do nas</a>
            </div>
        </div>
    </div>
</section>