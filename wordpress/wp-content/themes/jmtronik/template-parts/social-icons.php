<?php
/**
 * Template part for displaying social icons
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jmtronik
 */

?>

<div class="social-icons">
    <a href="<?php the_field('social_facebook', 'option'); ?>"><i class="fab fa-facebook-f"></i></a>
    <a href="<?php the_field('social_google', 'option'); ?>"><i class="fab fa-google-plus-g"></i></a>
    <a href="<?php the_field('social_twitter', 'option'); ?>"><i class="fab fa-twitter"></i></a>
    <a href="<?php the_field('social_youtube', 'option'); ?>"><i class="fab fa-youtube"></i></a>
</div>