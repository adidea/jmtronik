<section style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/fullwidth-bg.png')" class="container-fluid d-flex align-items-center fullwidth">
    <div class="row fullwidth__content">
        <div class="col-12 col-md-8 col-lg-4 mx-auto">
            <h4>
                <span class="heading--red"><?php the_field('products_head_red'); ?></span>
                <span class="heading--white"> <?php the_field('products_head_white'); ?></span>
            </h4>
            <a href="<?php the_field('products_link'); ?>" class="btn-std">Napisz do nas</a>
        </div>
    </div>
</section>
