<?php

get_header();
?>

<section class="container single-post py-5">
		<div class="row">
			<div class="col-12 col-lg-10 mx-auto">
			<?php

			// checks if there are any posts that match the query
			if (have_posts()) :

			// If there are posts matching the query then start the loop
			while ( have_posts() ) : the_post();

				// the code between the while loop will be repeated for each post
				?>

				<h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>

				<?php the_content(); ?>


				<?php

				// Stop the loop when all posts are displayed
			endwhile;

			// If no posts were found
			else :
			?>
			<p>Wybrany post nie istnieje</p>
			<?php
			endif;
			?>
			</div>
		</div>
	</section>

<?php
get_footer();
