<?php /* Template Name: Serwis */ ?>

<?php get_header(); ?>

<div class="container" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="col contact-p__breadcrumbs">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h2>
        </div>
    </div>
</div>

<section class="container service">
    <div class="row">
        <div class="col-md-5">
            <div class="service-p__wysiwyg">
                <?php the_field('service_p_wysiwyg'); ?>
            </div>
        </div>
        <div class="col-md-7">
            <?php echo do_shortcode('[contact-form-7 id="482" title="Strona serwis"]'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>