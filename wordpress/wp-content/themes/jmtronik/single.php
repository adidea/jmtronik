<?php
get_header();
?>
<div class="container breadcrumbs" style="padding-left: 0.8rem;">
	<?php if(function_exists('bcn_display'))
	{
		bcn_display();
	}?>
</div>

	<section class="container single__section py-5">
		<div class="row">
			<div class="col-12">
			<?php
			if (have_posts()) :
			while ( have_posts() ) : the_post();
				?>



				<div class="single__content">
					<div class="single__img-wrapper">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="single__date-wrapper">
						<h5><?php the_title(); ?></h5>
						<p>
							<span class="post-info--light-gray">
								<?php the_time(get_option('date_format')); ?> | by
							</span>
							<span class="post-info--dark-gray">
								<?php the_author(); ?>
							</span>
						</p>
					</div>

					<?php the_content(); ?>
				</div>

				<?php
			endwhile;

			// If no posts were found
			else :
			?>
			<p>Wybrany post nie istnieje</p>
			<?php
			endif;
			?>
			</div>
		</div>
	</section>

<?php
get_footer();

?>
