<?php
/*
 * Template Name: Realizacja - podstrona
 */

 get_header();  ?>

 <div class="container breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="offset-md-4 col-md-8 offset-lg-3 col-lg-9">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
        </div>
    </div>
</div>

<section class="container about">
    <div class="row">
        <div class="col-md-4 col-lg-3 about__nav">
            <ul class="nav nav-tabs about__nav-ul" id="nav-tab" role="tablist">
                <?php
                if( have_rows('realisations_tabs', 'option') ):
                    while ( have_rows('realisations_tabs', 'option') ) : the_row(); ?>
                        <li class="nav-item about__nav-li">
                            <a class="nav-link about__nav-a"
                            href="<?php the_sub_field('realisations_link'); ?>">
                                <?php the_sub_field('realisations_button'); ?>
                                <i class="fas fa-caret-right"></i>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>

        <div class="col-md-8 col-lg-9 about__content">
            <?php
            if (have_posts()) :
            while ( have_posts() ) : the_post();
                ?>
                <h2 class="my-3" style="margin-left: 8px"><?php the_title(); ?></h2>
                <div class="row realisation__wraper">
                    <div class="col-12 col-lg-4 d-flex align-items-center realisation__img-wrapper">
                        <?php the_post_thumbnail( 'full' ); ?>
                    </div>
                    <div class="col-12 col-lg-8 d-flex align-items-center realisation__content-wrapper">
                        <div class="realisation__text-wrapper">
                            <h4><?php the_title(); ?></h4>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php
            endwhile;
            else :
            ?>
            <p>Nie znaleziono postów</p>
            <?php
            endif;
            ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>

