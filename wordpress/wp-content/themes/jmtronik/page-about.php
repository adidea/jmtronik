<?php /* Template Name: O nas */ ?>

<?php get_header(); ?>

<div class="container breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="offset-md-4 col-md-8 offset-lg-3 col-lg-9">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h2>
        </div>
    </div>
</div>

<section class="container about">
    <div class="row">
        <div class="col-md-4 col-lg-3 about__nav">
            <ul class="nav nav-tabs about__nav-ul" id="nav-tab" role="tablist">
                <?php
                if( have_rows('about_tabs', 'option') ):
                    while ( have_rows('about_tabs', 'option') ) : the_row(); ?>
                        <li class="nav-item about__nav-li">
                            <a class="nav-link about__nav-a"
                            href="<?php the_sub_field('about_link'); ?>">
                                <?php the_sub_field('about_button'); ?>
                                <i class="fas fa-caret-right"></i>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>

        <div class="col-md-8 col-lg-9 about__content">
        <?php
        if( have_rows('row_text_only_before') ):
            while ( have_rows('row_text_only_before') ) : the_row();
            ?>
                <div class="row about__row mt-2 mb-3">
                    <div class="col-12 about__row--text-wrapper">
                        <?php the_sub_field('before'); ?>
                    </div>
                </div>

            <?php endwhile;
        else :
        endif;
        ?>

        <?php
            if( have_rows('about_rows') ):
                while ( have_rows('about_rows') ) : the_row();
                ?>
                <div class="row about__row mt-2 mb-3">
                    <div class="col-12 col-md-5 about__row--img-wrapper <?php if( get_sub_field('img_right') ): echo 'about__row--img-right'; endif;?>">
                        <?php $row_img = get_sub_field('row_img');
                        if( !empty($row_img) ):
                        ?>
                        <img src="<?php echo $row_img['url']; ?>" alt="<?php echo $row_img['alt']; ?>">
                        <?php endif; ?>
                    </div>
                    <div class="col-12 col-md-7 about__row--text-wrapper">
                        <?php the_sub_field('row_text'); ?>
                    </div>
                </div>


                <?php endwhile;
            else :
            endif;
            ?>

            <?php
        if( have_rows('row_text_only_after') ):
            while ( have_rows('row_text_only_after') ) : the_row();
            ?>
                <div class="row about__row mt-2 mb-3">
                    <div class="col-12 about__row--text-wrapper">
                        <?php the_sub_field('after'); ?>
                    </div>
                </div>

            <?php endwhile;
        else :
        endif;
        ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
