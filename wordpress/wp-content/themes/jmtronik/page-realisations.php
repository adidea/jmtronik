<?php /* Template Name: Realizacje */ ?>

<?php get_header(); ?>

<div class="container breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="offset-md-4 col-md-8 offset-lg-3 col-lg-9">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h3>
        </div>
    </div>
</div>

<section class="container about">
    <div class="row">
        <div class="col-md-4 col-lg-3 about__nav">
            <ul class="nav nav-tabs about__nav-ul" id="nav-tab" role="tablist">
                <?php
                if( have_rows('realisations_tabs', 'option') ):
                    while ( have_rows('realisations_tabs', 'option') ) : the_row(); ?>
                        <li class="nav-item about__nav-li">
                            <a class="nav-link about__nav-a"
                            href="<?php the_sub_field('realisations_link'); ?>">
                                <?php the_sub_field('realisations_button'); ?>
                                <i class="fas fa-caret-right"></i>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>

        <div class="col-md-8 col-lg-9 about__content">
            <?php
                if( have_rows('realisations_list') ):
                    while ( have_rows('realisations_list') ) : the_row();
                    ?>
                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 realisations__item-wrapper">
                        <a href="<?php the_sub_field('realisations_list_link'); ?>"><img src="<?php the_sub_field('realisations_list_img'); ?>" alt="<?php the_sub_field('realisations_list_alt'); ?>"></a>
                    </div>


                    <?php endwhile;
                endif;
                ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>