<?php /* Template Name: Strona główna */ ?>

<?php get_header(); ?>

<section class="container-fluid main__first-sec" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/main_page_bg.png');">
    <div class="container">
        <div class="row main__about">
            <div class="col-10 col-xs-9 col-sm-8 col-md-6 col-lg-5 main__about-fig d-flex align-items-center">
                <div class="main__about-fig--content">
                    <span class="main__about-fig--text1">
                        <?php the_field('main_slogan_small'); ?>
                    </span>
                    <span class="main__about-fig--text2">
                        <?php the_field('main_slogan_big'); ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="caontainer-fluid main__second-sec">
    <div class="container">
        <div class="row main__about2">
            <div class="col-xs-12 offset-md-4 col-md-8 offset-lg-5 col-lg-7 offset-xl-6 col-xl-6">
                <h1><span class="heading--red"><?php the_field('main_sec_1_heading_red'); ?> </span><?php the_field('main_sec_1_heading'); ?></h1>
                <a href="<?php the_field('main_sec_1_btn_red'); ?>" class="btn-std mb-5">Napisz do nas</a>
            </div>
            <div class="col-xs-12 offset-md-2 col-md-10 offset-lg-3 col-lg-9 offset-xl-4 col-xl-8">
                <h2 class="mb-5"><?php the_field('main_sec_1_heading_about'); ?></h2>
                <p><?php the_field('main_sec_1_text_about'); ?></p>
                <div class="d-flex mb-4 mt-4"><a href="<?php the_field('main_sec_1_btn_more'); ?>" class="btn--arrow ml-auto">czytaj więcej</a><i class="far fa-arrow-alt-circle-right"></i></div>
            </div>
        </div>
    </div>
</section>
<section class="container-fluid main__third-sec">
    <div class="container main__third-sec-wrapper">
        <h2>Nasze produkty</h2>
        <div class="row main__products-wrapper">
            <?php
                // check if the repeater field has rows of data
                if( have_rows('main_sec_2_products') ):
                    // loop through the rows of data
                    while ( have_rows('main_sec_2_products') ) : the_row();
                    ?>
                        <div class="col-12 col-sm-6 col-md-4 col-lg main__product-wrapper">
                            <div class="main__product--img-wrapper">
                                <img src="<?php the_sub_field('main_sec_2_product_img'); ?>" alt="">
                            </div>
                            <div class="main__product--link-wrapper">
                                <a href="<?php the_sub_field('main_sec_2_product_link'); ?>">
                                    <h3><?php the_sub_field('main_sec_2_product_head'); ?></h3>
                                </a>
                            </div>
                        </div>
                    <?php endwhile;
                else :
                    // no rows found
                endif;
            ?>
        </div>
    </div>
</section>
<section class="container-fluid main__fourth-sec" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/main_bg_2.png')">
    <div class="row main__fourth--slogan-wrapper">
        <div class="offset-3 col-4 offset-md-8 col-md-3 main__fourth--slogan">
            <h2><?php the_field('main_sec_3_slogan'); ?></h2>
        </div>
    </div>
</section>

<section class="container-fluid main__fifth-sec">
    <h4 class="pb-5 pt-5 heading--red section--title">AKTUALNOŚCI</h4>
    <div class="container main__articles-wrapper pb-5 pt-4">
        <?php $catquery = new WP_Query( 'cat=3&posts_per_page=2' ); ?>
                <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                <div class="row main__article-wrapper py-5">
                <div class="col-md-10 col-md-6 main__article pl-4 pt-3">
                <h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                <p><?php the_excerpt(); ?></p>
                </div>
            </div>
                <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    </div>
</section>
<section class="container-fluid main__sixth-sec">
    <div class="row">
        <div class="col-12 col-xl-8 main__sixth-slider">
            <div class="owl-carousel">
            <?php
                // check if the repeater field has rows of data
                if( have_rows('main_slider') ):
                    // loop through the rows of data
                    while ( have_rows('main_slider') ) : the_row();
                    ?>
                        <img src="<?php the_sub_field('main_slider_img'); ?>" alt="<?php the_sub_field('main_slider_alt'); ?>">
                    <?php endwhile;
                else :
                    // no rows found
                endif;
            ?>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('template-parts/contact-section'); ?>

<?php get_footer(); ?>