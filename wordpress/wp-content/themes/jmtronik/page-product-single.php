<?php /* Template Name: Produkt */ ?>

<?php get_header(); ?>

<div class="container breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <div class="row">
        <div class="offset-md-3 col-md-9 offset-lg-2 col-lg-10">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
            <h2 class="my-3"><?php the_title(); ?></h2>
        </div>
    </div>
</div>

<section class="container-fluid about product__container">
    <div class="row">
        <div class="col-md-3 col-lg-2 about__nav">
            <ul class="nav nav-tabs about__nav-ul" id="nav-tab" role="tablist">
                <?php
                if( have_rows('products_tabs', 'option') ):
                    while ( have_rows('products_tabs', 'option') ) : the_row(); ?>
                        <li class="nav-item about__nav-li">
                            <a class="nav-link about__nav-a"
                            href="<?php the_sub_field('products_link'); ?>">
                                <?php the_sub_field('products_button'); ?>
                                <i class="fas fa-caret-right"></i>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>

        <div class="col-md-9 col-lg-10 about__content">
            <div class="col-xl-4 product__gallery">
                <div class="slider slider-single">
                <?php
                if( have_rows('product_gallery') ):
                    while ( have_rows('product_gallery') ) : the_row();
                    ?>
                        <div><img src="<?php the_sub_field('product_gallery_img'); ?>" alt="<?php the_sub_field('product_gallery_alt'); ?>"></div>
                    <?php endwhile;
                endif;
                ?>
                </div>
                <div class="slider slider-nav">
                    <?php
                    if( have_rows('product_gallery') ):
                        while ( have_rows('product_gallery') ) : the_row();
                        ?>
                            <div><img src="<?php the_sub_field('product_gallery_img'); ?>" alt="<?php the_sub_field('product_gallery_alt'); ?>"></div>
                        <?php endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-xl-8 product__content">
            <?php
            if (have_posts()) :
            while ( have_posts() ) : the_post();
                ?>
                    <?php the_content(); ?>
                <?php
            endwhile;
            // If no posts were found
            else :
            ?>
            <p>Sorry no posts matched your criteria.</p>
            <?php
            endif;
            ?>
            <div class="product__btns-wrapper">
                <a href="<?php the_field('product_btn_ask'); ?>" class="btn-std">Złóż zapytanie</a>
                <a href="<?php the_field('product_btn_pdf'); ?>" target="_blank" class="product__pdf-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/pdf-icon.png" alt=""></a>
            </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part( 'template-parts/full-width-img' ) ?>

<?php get_footer(); ?>
