<?php
/**
 * @package jmtronik
 */
?>

	<footer class="container-fluid footer">
		<div class="container footer__content">
			<div class="row">
				<a href="<?php echo home_url(); ?>">
					<img style="footer__logo" src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="jm tronik logo" class="footer__logo">
				</a>
			</div>
			<div class="row d-flex align-items-end">
				<div class="col-6 col-md-4 col-lg-3 footer__block">
					<?php the_field('footer_addres', 'option'); ?>
				</div>
				<div class="col-6 col-md-3 col-lg-2 footer__block">
					<?php the_field('footer_contact', 'option'); ?>
				</div>
				<div class="col-6 col-md-3 col-lg-2 footer__block footer__nav">
					<?php
					// check if the repeater field has rows of data
					if( have_rows('footer_menu_links_1', 'option') ):

						// loop through the rows of data
						while ( have_rows('footer_menu_links_1', 'option') ) : the_row();
						?>
							<a class="footer__nav-link" href="<?php the_sub_field('footer_menu_link'); ?>"><?php the_sub_field('footer_menu_link_desc'); ?></a>
						<?php endwhile;
					else :
						// no rows found
					endif;
					?>
				</div>
				<div class="col-6 col-md-2 col-lg-2 footer__block footer__nav">
					<?php
					// check if the repeater field has rows of data
					if( have_rows('footer_menu_links_2', 'option') ):

						// loop through the rows of data
						while ( have_rows('footer_menu_links_2', 'option') ) : the_row();
						?>
							<a class="footer__nav-link" href="<?php the_sub_field('footer_menu_link'); ?>"><?php the_sub_field('footer_menu_link_desc'); ?></a>
						<?php endwhile;
					else :
						// no rows found
					endif;
					?>
				</div>
				<div class="col-lg-3 footer__block">
					<div class="footer__social-icons">
						<?php get_template_part( 'template-parts/social-icons' ) ?>
					</div>
					<span style="font-size: 0.7rem; color: #ffffff">Copywright © JM-Tronik Sp. z o.o., Designed by <a href="http://vilaro.pl/" style="color: #fff;">Vilaro</a> 2018</span>
				</div>
			</div>
		</div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>
