'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function(){
    return gulp.src('scss/main.scss')
      .pipe(sass()) // Using gulp-sass
      .pipe(gulp.dest('wordpress/wp-content/themes/jmtronik/css'))
  });

gulp.task('prefix', () =>
gulp.src('wordpress/wp-content/themes/jmtronik/css/main.css')
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('wordpress/wp-content/themes/jmtronik/css/prefi/'))
);

gulp.task('default', function () {
gulp.watch('scss/**/*.scss', ['sass']);
gulp.watch('wordpress/wp-content/themes/jmtronik/css/*.css', ['prefix']);
});


